﻿How to Build ChyronHego.Enterprise.Presentation.Test

1. Clone ChyronHego.Enterprise.Presentation from Bitbucket (https://bitbucket.org/chyronhego/chyronhego.enterprise.presentation)
2. Clone Nuget Packages (https://bitbucket.org/chyronhego/nuget)
3. Go to Tools > Options > Nuget Package Manager. Add Pacakage Source and point to Nuget Pacakges Repo
4. Set ChyronHego.Enterprise.Presentation.Test as Startup Project
