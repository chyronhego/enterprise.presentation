﻿namespace ChyronHego.Enterprise.Presentation.Collections
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;

    public class ObservableCollectionEx<T> : ObservableCollection<T>
    {
        public ObservableCollectionEx() { }

        public ObservableCollectionEx(IEnumerable<T> collection) : this()
        {
            AddRange(collection);
        }

        public void AddRangeWithMaxCount(IEnumerable<T> items, int maxCount)
        {
            if (maxCount < 0)
            {
                maxCount = 0;
            }

            foreach (var item in items)
            {
                AddInternalItem(item);
            }

            while (Items.Count > maxCount)
            {
                var item = Items[0];
                RemoveInternalItem(item);
            }

            OnPropertyChanged(new PropertyChangedEventArgs("Count"));
            OnPropertyChanged(new PropertyChangedEventArgs("Item[]"));
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public void AddRange(IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                AddInternalItem(item);
            }

            OnPropertyChanged(new PropertyChangedEventArgs("Count"));
            OnPropertyChanged(new PropertyChangedEventArgs("Item[]"));
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public void ClearAddRange(IEnumerable<T> list)
        {
            ClearInternalItems();
            AddRange(list);
        }

        protected virtual void AddInternalItem(T item)
        {
            Items.Add(item);
        }

        protected virtual void RemoveInternalItem(T item)
        {
            Items.Remove(item);
        }

        protected virtual void ClearInternalItems()
        {
            Items.Clear();
        }
    }
}