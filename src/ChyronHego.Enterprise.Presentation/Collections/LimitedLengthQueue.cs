﻿namespace ChyronHego.Enterprise.Presentation.Collections
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using ChyronHego.Enterprise.Infrastructure.Objects;

    public class LimitedLengthQueue<T> : IEnumerable<T>
    {
        protected ConcurrentList<T> items;

        public int Limit { get; }

        public event Action QueueChanged;
        public event Action QueueCleared;    

        public LimitedLengthQueue(int limit)
        {
            Limit = limit;
            items = new ConcurrentList<T>();
        }

        public virtual void Enqueue(T value)
        {
            items.Add(value);

            while (items.Count > Limit)
            {
                RemoveAt(0);
            }

            QueueChanged?.Invoke();
        }

        protected virtual void RemoveAt(int index)
        {
            items.RemoveAt(index);
        }

        public virtual void Clear()
        {
            items.Clear();

            QueueChanged?.Invoke();

            QueueCleared?.Invoke();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return items.GetEnumerator();
        }
    }
}
