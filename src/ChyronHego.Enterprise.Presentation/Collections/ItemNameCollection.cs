﻿namespace ChyronHego.Enterprise.Presentation.Collections
{
    using System.Collections.ObjectModel;
    using ChyronHego.Enterprise.Infrastructure.Xml;

    public class ItemNameCollection<T> : ObservableCollection<T>, IXmlPropertyNameConverter
    {
        public virtual string ItemName { get; set; }

        string IXmlPropertyNameConverter.GetXmlPropertyName(string name, object value)
        {
            if (name == "Item")
            {
                return ItemName;
            }

            return name;
        }

        void IXmlPropertyNameConverter.SetXmlPropertyName(XmlTypeNode child)
        {
            if (child.Name == ItemName)
            {
                child.Name = "Item";
            }
        }
    }
}
