﻿namespace ChyronHego.Enterprise.Presentation
{
    using System.Windows.Threading;
    using ChyronHego.Enterprise.Logging;

    public class WpfExceptionHandler
    {
        private static WpfExceptionHandler exceptionHandler;

        public static void Initialize(Dispatcher dispatcher)
        {
            if (exceptionHandler == null)
            {
                exceptionHandler = new WpfExceptionHandler(dispatcher);               
            }
        }

        public WpfExceptionHandler(Dispatcher dispatcher)
        {
            dispatcher.UnhandledException += Dispatcher_UnhandledException;
        }

        private void Dispatcher_UnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            LogManager.Log("Browser", e.Exception);
        }
    }
}
