﻿namespace ChyronHego.Enterprise.Presentation.Objects
{
    using System.Windows;

    /// <summary>
    /// Main use case for this is to allow the user to use XAML binding to create a static resource which points to
    /// the data context of a given object.  This, in turn, allows the user to bind to that data context from objects
    /// that do not participate in the visual tree of the parent control (such as Context Menu and CollectionContainer)
    /// usage
    /// XAML declaration: <BindingProxy x:Key="proxy" Data="{Binding}"></BindingProxy>
    /// XAML binding: <MenuItem Header="Save" Command="{Binding Source={StaticResource proxy}, Path=Data.SomeCommand}"></MenuItem>
    /// </summary>
    public class BindingProxy : Freezable
    {
        protected override Freezable CreateInstanceCore()
        {
            return new BindingProxy();
        }

        public object Data
        {
            get => (object)GetValue(DataProperty);
            set => SetValue(DataProperty, value);
        }

        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("Data",
                typeof(object),
                typeof(BindingProxy),
                new UIPropertyMetadata(null));
    }
}
