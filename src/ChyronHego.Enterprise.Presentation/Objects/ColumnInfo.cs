﻿namespace ChyronHego.Enterprise.Presentation.Objects
{
    using System.ComponentModel;

    public class ColumnInfo : INotifyPropertyChanged
    {
        private bool visible;
        public bool CanHide { get; set; }
        public bool CanReorder { get; set; }
        public string Name { get; set; }
        public string BindingPath { get; set; }
        public double InitialWidth { get; set; }
        public int DisplayIndex { get; set; }
        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
                OnPropertyChanged("Visible");
            }
        }

        public int DisplayBindingIndex { get; set; }
        public string Text { get; set; }
        public CompareDelegate Compare { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        public delegate int CompareDelegate(object x, object y);
    }
}