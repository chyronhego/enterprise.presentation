﻿namespace ChyronHego.Enterprise.Presentation.ViewModels
{
    using ChyronHego.Enterprise.Presentation.Collections;
    using ChyronHego.Enterprise.Presentation.Objects;   
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;

    public class BrowserViewModel : BaseViewModel
    {
        private bool loading;
        private BrowserItem selectedItem;
        private ObservableCollectionEx<QualityControlInfo> qualityControlOptions;
        private string longestName = "";

        public ObservableCollectionEx<ColumnInfo> Columns { get; set; }

        public ObservableCollectionEx<BrowserItem> Items { get; set; }

        public ObservableCollectionEx<BrowserItem> SelectedItems { get; set; }

        public BrowserItem SelectedItem
        {
            get
            {
                return selectedItem;
            }
            set
            {
                if (selectedItem != value)
                {
                    selectedItem = value;
                    OnPropertyChanged("SelectedItem");
                }
            }
        }

        public bool Loading
        {
            get
            {
                return loading;
            }
            set
            {
                if (loading != value)
                {
                    loading = value;
                    OnPropertyChanged("Loading");
                }
            }
        }

        public BrowserViewModel()
        {
            Columns = new ObservableCollectionEx<ColumnInfo>();
            Items = new ObservableCollectionEx<BrowserItem>();            
            SelectedItems = new ObservableCollectionEx<BrowserItem>();
            Items.CollectionChanged += Items_CollectionChanged;
            QualityControlOptions = new ObservableCollectionEx<QualityControlInfo>();
            Sorter = new BrowserItemCustomSorter(this);
            Loading = true;
        }

        private void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                case NotifyCollectionChangedAction.Replace:
                    var longestNameItem = e.NewItems.Cast<BrowserItem>().OrderByDescending(item => item.Name.Length).FirstOrDefault();
                    if (longestNameItem?.Name.Length > LongestName.Length)
                    {
                        LongestName = longestNameItem.Name;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    var removingLongestItem = e.OldItems.Cast<BrowserItem>().Any(browserItem => browserItem.Name == LongestName);
                    if (removingLongestItem)
                    {
                        UpdateLongestName();
                    }
                    break;
                case NotifyCollectionChangedAction.Reset:
                    UpdateLongestName();
                    break;
            }
        }

        public ObservableCollectionEx<QualityControlInfo> QualityControlOptions
        {
            get
            {
                return qualityControlOptions;
            }
            set
            {
                if (qualityControlOptions != value)
                {
                    qualityControlOptions = value;
                    OnPropertyChanged("QualityControlOptions");
                }
            }
        }

        public string LongestName
        {
            get
            {
                return longestName;
            }

            set
            {
                if (longestName != value)
                {
                    longestName = value;
                    OnPropertyChanged("LongestName");
                }
            }
        }

        public BrowserItem RenamingItem { get; private set; }
        public BrowserItemCustomSorter Sorter { get; internal set; }

        public void RenameItem(BrowserItem item)
        {
            item.Renaming = true;
            RenamingItem = item;
        }

        internal void UpdateLongestName()
        {
            LongestName = Items.OrderByDescending(item => item.Name.Length).FirstOrDefault()?.Name ?? "";
        }
    }
}