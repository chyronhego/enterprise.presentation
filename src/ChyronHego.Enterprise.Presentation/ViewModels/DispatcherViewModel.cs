﻿namespace ChyronHego.Enterprise.Presentation.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Windows.Threading;

    public class DispatcherViewModel : INotifyPropertyChanged
    {
        private readonly Dispatcher dispatcher;

        public event PropertyChangedEventHandler PropertyChanged;

        public DispatcherViewModel()
        {
            dispatcher = Dispatcher.CurrentDispatcher;
        }

        protected void RaisePropertyChanged(string property)
        {
            if (!dispatcher.CheckAccess())
            {
                dispatcher.BeginInvoke(new Action<string>(RaisePropertyChangedInternal), property);
            }
            else
            {
                RaisePropertyChangedInternal(property);
            }
        }

        private void RaisePropertyChangedInternal(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        protected void ThreadInvoke(Action action)
        {
            if (!dispatcher.CheckAccess())
            {
                dispatcher.BeginInvoke(new Action(action));
            }
            else
            {
                action();
            }
        }
    }
}
