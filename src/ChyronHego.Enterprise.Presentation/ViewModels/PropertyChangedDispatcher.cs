﻿namespace ChyronHego.Enterprise.Presentation.ViewModels
{
    using System;
    using System.ComponentModel;
    using System.Windows.Threading;

    public class PropertyChangedDispatcher
    {
        private readonly Dispatcher dispatcher;
        private readonly Action<PropertyChangedEventHandler, object, string> invokeHandler;

        public PropertyChangedDispatcher()
        {
            dispatcher = Dispatcher.CurrentDispatcher;
            invokeHandler = new Action<PropertyChangedEventHandler, object, string>(RaisePropertyChangedInternal);
        }

        public void ThreadInvoke(Action action)
        {
            if (!dispatcher.CheckAccess())
            {
                dispatcher.BeginInvoke(new Action(action));
            }
            else
            {
                action();
            }
        }

        private void RaisePropertyChangedInternal(PropertyChangedEventHandler handler, object sender, string property)
        {
            handler?.Invoke(sender, new PropertyChangedEventArgs(property));
        }
        
        public void RaisePropertyChanged(PropertyChangedEventHandler handler, object sender, string property)
        {
            if (!dispatcher.CheckAccess())
            {
                dispatcher.BeginInvoke(invokeHandler, handler, sender, property);
            }
            else
            {
                RaisePropertyChangedInternal(handler, sender, property);
            }
        }
    }
}
