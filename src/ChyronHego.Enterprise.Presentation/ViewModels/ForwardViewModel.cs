﻿namespace ChyronHego.Enterprise.Presentation.ViewModels
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using ChyronHego.Enterprise.Presentation.ViewModels.Bindings;

    public class ForwardViewModel : DispatcherViewModel
    {
        private INotifyPropertyChanged model;

        private readonly Dictionary<string, string> propertyTable = new Dictionary<string, string>();
        private readonly Dictionary<string, CommandBinding> commandTable = new Dictionary<string, CommandBinding>();

        public ForwardViewModel()
        {
        }

        public ForwardViewModel(INotifyPropertyChanged model)
        {
            Model = model;
        }

        public INotifyPropertyChanged Model
        {
            get => model;
            set
            {
                if (model != null)
                {
                    DetachPropertyChangedModels();
                }

                model = value;

                if (model != null)
                {
                    foreach (string property in propertyTable.Values)
                    {
                        RaisePropertyChanged(property);
                    }

                    foreach (var command in commandTable.Values)
                    {
                        ThreadInvoke(command.UpdateCanExecute);
                    }

                    AttachPropertyChangedModels();
                }
            }
        }

        protected virtual IEnumerable<INotifyPropertyChanged> GetPropertyChangedModels()
        {
            yield return model;
        }

        private void AttachPropertyChangedModels()
        {
            foreach (var model in GetPropertyChangedModels())
            {
                model.PropertyChanged += Model_PropertyChanged;
            }
        }

        private void DetachPropertyChangedModels()
        {
            foreach (var model in GetPropertyChangedModels())
            {
                model.PropertyChanged -= Model_PropertyChanged;
            }
        }

        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (propertyTable.TryGetValue(e.PropertyName, out string viewModelProperty))
            {
                RaisePropertyChanged(viewModelProperty);
            }
        }

        protected void AddProperty(string property)
        {
            AddProperty(property, property);
        }

        protected void AddProperty(string modelProperty, string viewModelProperty)
        {
            propertyTable[modelProperty] = viewModelProperty;
        }

        protected void AddCommand(string viewModelCommand, CommandBinding command)
        {
            commandTable[viewModelCommand] = command;
        }
    }

    public class ForwardViewModel<T> : ForwardViewModel where T : INotifyPropertyChanged
    {
        protected T model;

        public ForwardViewModel()
        {
        }

        public ForwardViewModel(T model)
        {
            Model = model;
        }

        public new T Model
        {
            get => model;
            set
            {
                model = value;

                base.Model = value;
            }
        }
    }
}
