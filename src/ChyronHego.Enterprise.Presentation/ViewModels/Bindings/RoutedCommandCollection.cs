﻿namespace ChyronHego.Enterprise.Presentation.ViewModels.Bindings
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Windows.Input;

    public class RoutedCommandCollection<T>
    {
        private static T instance;

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = Activator.CreateInstance<T>();
                }

                return instance;
            }
        }

        private readonly Dictionary<string, RoutedUICommand> commandList = new Dictionary<string, RoutedUICommand>();

        protected virtual InputGestureCollection BuildInputGestures(params string[] inputGestures)
        {
            if (inputGestures == null)
            {
                return null;
            }

            var result = new InputGestureCollection();

            var gestureConverter = new KeyGestureConverter();

            foreach (var inputStringGesture in inputGestures)
            {
                var inputGesture = gestureConverter.ConvertFromString(inputStringGesture) as InputGesture;
                
                //if any of the gestures cannot convert then we balk at the entire collection
                if (inputGesture == null)
                {
                    return null;
                }

                result.Add(inputGesture);
            }

            return result;
        }

        protected virtual RoutedUICommand EnsureCommand(string displayText = "", [CallerMemberName] string commandId = "", params string[] inputGestures)
        {
            if (string.IsNullOrWhiteSpace(commandId))
            {
                return null;
            }

            if (string.IsNullOrWhiteSpace(displayText))
            {
                displayText = commandId;
            }

            var inputGestureCollection = BuildInputGestures(inputGestures);

            lock (commandList)
            {
                if (!commandList.ContainsKey(commandId))
                {
                    if (inputGestureCollection != null)
                    {
                        commandList.Add(commandId, new RoutedUICommand(displayText, commandId, typeof(T), inputGestureCollection));
                    }
                    else
                    {
                        commandList.Add(commandId, new RoutedUICommand(displayText, commandId, typeof(T)));
                    }
                }

                return commandList[commandId];
            }
        }
    }
}
