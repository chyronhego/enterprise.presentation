﻿namespace ChyronHego.Enterprise.Presentation.ViewModels.Bindings
{
    using System;
    using System.Windows.Input;

    public class CommandBinding : ICommand
    {
        private readonly Action execute;
        private readonly Func<bool> canExecute;

        public event EventHandler CanExecuteChanged;

        public CommandBinding(Action execute)
            : this(execute, () => true)
        {
        }

        public CommandBinding(Action execute, Func<bool> canExecute)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return canExecute();
        }

        public void Execute(object parameter)
        {
            execute();
        }

        public void UpdateCanExecute()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    public class CommandBinding<T> : ICommand where T : class
    {
        private readonly Action<T> execute;
        private readonly Func<T, bool> canExecute;

        public CommandBinding(Action<T> DoExecute) : this(DoExecute, (T) => true)
        {

        }

        public CommandBinding(Action<T> execute, Func<T, bool> canExecute)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return CanExecute(parameter as T);
        }

        public bool CanExecute(T parameter)
        {
            return canExecute?.Invoke(parameter) ?? true;
        }

        public void Execute(object parameter)
        {
            Execute(parameter as T);
        }

        public void Execute(T parameter)
        {
            execute?.Invoke(parameter);
        }

        public void UpdateCanExecute()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
