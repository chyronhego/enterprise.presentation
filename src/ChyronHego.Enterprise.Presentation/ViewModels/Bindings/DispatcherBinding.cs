﻿namespace ChyronHego.Enterprise.Presentation.ViewModels.Bindings
{
    using System;
    using System.Windows.Threading;
    using ChyronHego.Enterprise.Presentation.Models.Bindings;

    public class DispatcherBinding : NotifyBinding
    {
        private readonly Dispatcher dispatcher;

        public DispatcherBinding(string sourceProperty, Action updateDestination)
            : base(sourceProperty, updateDestination)
        {
            if (updateDestination.Target is DispatcherObject dispatcherObject)
            {
                dispatcher = dispatcherObject.Dispatcher;
            }
        }

        protected override void OnUpdateDestination()
        {
            if (dispatcher != null && updateDestination != null)
            {
                dispatcher.BeginInvoke(updateDestination);
            }
            else
            {
                base.OnUpdateDestination();
            }
        }
    }
}
