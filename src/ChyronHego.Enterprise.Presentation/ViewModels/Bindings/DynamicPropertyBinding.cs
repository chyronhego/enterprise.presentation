﻿namespace ChyronHego.Enterprise.Presentation.ViewModels.Bindings
{
    using System;

    public class DynamicPropertyBinding
    {
        public DynamicPropertyBinding(string modelPropertyName, Func<object> getModelValue, Action<object> setModelValue, string viewPropertyName)
        {
            ModelPropertyName = modelPropertyName;

            GetModelValue = getModelValue;

            ViewPropertyName = viewPropertyName;

            SetModelValue = setModelValue;
        }

        public string ModelPropertyName { get; private set; }

        public Func<object> GetModelValue { get; private set; }

        public Action<object> SetModelValue { get; private set; }

        public string ViewPropertyName { get; private set; }
    }
}
