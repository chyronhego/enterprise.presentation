﻿namespace ChyronHego.Enterprise.Presentation.ViewModels.Components
{
    public class ClickableArea
    {
        private double cornerRadiusCoefficient;

        public ClickableArea(double elementWidth, double displayedElementWidth, double cornerRadiusCoefficient = 0)
        {
            this.cornerRadiusCoefficient = cornerRadiusCoefficient;
            ElementWidth = elementWidth;
            DisplayedElementWidth = displayedElementWidth;
        }

        public double ElementWidth { get; private set; }

        public double DisplayedElementWidth { get; private set; }

        public double ClickableAreaThickness
        {
            get => ElementWidth / 2 - DisplayedElementWidth / 2;
        }

        public double Margin
        {
            get => ElementWidth / -2;
        }

        public double CornerRadius
        {
            get => cornerRadiusCoefficient <= double.Epsilon ? 0 : ElementWidth / cornerRadiusCoefficient;
        }
    }
}
