﻿namespace ChyronHego.Enterprise.Presentation.ViewModels
{
    using System.Runtime.CompilerServices;
    using ChyronHego.Enterprise.Presentation.Models;

    public class NotifyViewModel : NotifyModel
    {
        protected readonly PropertyChangedDispatcher dispatcher = new PropertyChangedDispatcher();

        protected override void OnPropertyChanged([CallerMemberName]string property = "")
        {
            dispatcher.RaisePropertyChanged(PropertyChangedHandler, this, property);
        }
    }
}
