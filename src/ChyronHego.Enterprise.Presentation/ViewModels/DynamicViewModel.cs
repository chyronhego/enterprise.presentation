﻿namespace ChyronHego.Enterprise.Presentation.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Dynamic;
    using System.Reflection;
    using System.Windows.Threading;
    using ChyronHego.Enterprise.Presentation.Models;
    using ChyronHego.Enterprise.Presentation.ViewModels.Bindings;

    public abstract class DynamicViewModel<T> : DynamicObject, INotifyPropertyChanged where T : INotifyTable
    {
        private event PropertyChangedEventHandler propertyChanged;
        private event PropertyChangedEventHandler dispatcherPropertyChanged;

        private struct Property
        {
            public Property(Type type, string modelProperty, string viewModelProperty)
            {
                Type = type;
                ModelProperty = modelProperty;

                ViewModelProperties = new List<string>
                {
                    viewModelProperty
                };
            }

            public Type Type { get; }

            public string ModelProperty { get; }

            public List<string> ViewModelProperties { get; }
        }


        private readonly Dictionary<string, Property> propertyTable = new Dictionary<string, Property>();
        private readonly Dictionary<string, object> commandTable = new Dictionary<string, object>();

        private T model;

        private Dispatcher dispatcher;

        public DynamicViewModel()
        {
            Initialize();
        }

        protected DynamicViewModel(T model)
        {
            Model = model;

            Initialize();
        }

        public T Model
        {
            get => model;
            set
            {
                if (model != null)
                {
                    DetachPropertyChangedModels();
                }

                OnModelUpdated(value);

                if (model != null)
                {
                    AttachPropertyChangedModels();
                }
            }
        }

        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            add
            {
                if (value.Target is PropertyChangedEventManager manager)
                {
                    if (dispatcher == null)
                    {
                        dispatcher = manager.Dispatcher;
                    }

                    dispatcherPropertyChanged += value;
                }
                else
                {
                    propertyChanged += value;
                }
            }
            remove
            {
                if (value.Target is PropertyChangedEventManager)
                {
                    dispatcherPropertyChanged -= value;

                    if (dispatcherPropertyChanged == null && dispatcher != null)
                    {
                        dispatcher = null;
                    }
                }
                else
                {
                    propertyChanged -= value;
                }
            }
        }

        protected virtual void OnModelUpdated(T model)
        {
            this.model = model;

            UpdateBindings();
        }

        protected virtual IEnumerable<INotifyPropertyChanged> GetPropertyChangedModels()
        {
            yield return model;
        }

        protected void AttachPropertyChangedModels()
        {
            foreach (var model in GetPropertyChangedModels())
            {
                model.PropertyChanged += Model_PropertyChanged;
            }
        }

        protected void DetachPropertyChangedModels()
        {
            foreach (var model in GetPropertyChangedModels())
            {
                model.PropertyChanged -= Model_PropertyChanged;
            }
        }

        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            RaisePropertyChanged(e.PropertyName);
        }

        private void RaisePropertyChanged(string modelProperty)
        {
            if (propertyTable.TryGetValue(modelProperty, out Property property))
            {
                foreach (string viewModelProperty in property.ViewModelProperties)
                {
                    propertyChanged?.Invoke(this, new PropertyChangedEventArgs(viewModelProperty));

                    if (dispatcher != null)
                    {
                        dispatcher.BeginInvoke(new Action<string>(RaiseDispatcherPropertyChanged), viewModelProperty);
                    }
                }
            }
        }

        protected void RaiseDispatcherPropertyChanged(string property)
        {
            dispatcherPropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        private void Initialize()
        {
            InitializeDynamicBindings();

            InitializeBindings();
        }

        private void InitializeDynamicBindings()
        {
            if (model != null)
            {
                foreach (var property in model.Properties)
                {
                    AddProperty(property.Value.GetType(), property.Key);
                }
            }
            else
            {
                foreach (var propertyInfo in typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance))
                {
                    var propertyType = propertyInfo.PropertyType;

                    if (propertyInfo.CanRead && propertyInfo.CanWrite)
                    {
                        AddProperty(propertyType, propertyInfo.Name);
                    }
                }
            }
        }

        protected virtual void InitializeBindings()
        {
        }

        private void AddProperty(Type type, string property)
        {
            AddProperty(type, property, property);
        }

        protected void AddProperty(string modelProperty, string viewModelProperty)
        {
            if (model != null && model.TryGetValue(modelProperty, out object value))
            {
                AddProperty(value.GetType(), modelProperty, viewModelProperty);
            }
            else if (propertyTable.TryGetValue(modelProperty, out Property property))
            {
                property.ViewModelProperties.Add(viewModelProperty);
            }
        }

        private void AddProperty(Type type, string modelProperty, string viewModelProperty)
        {
            if (!propertyTable.TryGetValue(modelProperty, out Property property))
            {
                property = new Property(type, modelProperty, viewModelProperty);

                propertyTable[modelProperty] = property;
            }
            else if (!property.ViewModelProperties.Contains(viewModelProperty))
            {
                property.ViewModelProperties.Add(viewModelProperty);
            }
        }

        protected void AddCommand(string viewModelCommand, Action action)
        {
            AddCommand(viewModelCommand, new CommandBinding(action));
        }

        protected void AddCommand(string viewModelCommand, CommandBinding command)
        {
            commandTable[viewModelCommand] = command;
        }

        protected void UpdateBindings()
        {
            foreach (string modelProperty in propertyTable.Keys)
            {
                RaisePropertyChanged(modelProperty);
            }
        }

        public override IEnumerable<string> GetDynamicMemberNames()
        {
            foreach (string name in base.GetDynamicMemberNames())
            {
                yield return name;
            }

            foreach (string name in propertyTable.Keys)
            {
                yield return name;
            }
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            if (!base.TryGetMember(binder, out result) && model?.TryGetValue(binder.Name, out result) != true)
            {
                if (propertyTable.TryGetValue(binder.Name, out Property property))
                {
                    result = Activator.CreateInstance(property.Type);
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            if (!base.TrySetMember(binder, value))
            {
                model?.SetValue(binder.Name, value);
            }

            return true;
        }
    }
}
