﻿namespace ChyronHego.Enterprise.Presentation.ViewModels
{
    using System.Collections.Generic;
    using ChyronHego.Enterprise.Presentation.Collections;
    using ChyronHego.Enterprise.Presentation.Objects;

    public class LogViewModel : BaseViewModel
    {
        public ObservableCollectionEx<LogItem> Items { get; set; } = new ObservableCollectionEx<LogItem>();

        public ObservableCollectionEx<ColumnInfo> Columns { get; set; } = new ObservableCollectionEx<ColumnInfo>();

        public int MaximumItemCount { get; set; } = 1000;

        public void AddLogItems(IEnumerable<LogItem> items)
        {
            Items.AddRangeWithMaxCount(items, MaximumItemCount);
        }

        public void AddLogItem(LogItem item)
        {
            Items.AddRangeWithMaxCount(new List<LogItem> { item }, MaximumItemCount);
        }

        public void ClearItems()
        {
            Items.Clear();
        }

        public LogItem CreateEmptyLogItem()
        {
            var logItem = new LogItem();
            for (int index = 0; index < Columns.Count; index++)
            {
                logItem.Add("");
            }

            return logItem;
        }
    }
}