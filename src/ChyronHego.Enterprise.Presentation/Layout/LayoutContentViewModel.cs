﻿namespace ChyronHego.Enterprise.Presentation.Layout
{
    using ChyronHego.Enterprise.Presentation.Interfaces;
    using ChyronHego.Enterprise.Presentation.ViewModels;

    public class LayoutContentViewModel : NotifyViewModel, ILayoutContentViewModel
    {
        public string ContentId
        {
            get => Get<string>();
            set => Set(value);
        }

        public string Title
        {
            get => Get<string>();
            set => Set(value);
        }

        public bool IsSelected
        {
            get => Get<bool>();
            set => Set(value);
        }

        public bool IsActive
        {
            get => Get<bool>();
            set => Set(value);
        }

        public bool IsVisible
        {
            get => Get<bool>();
            set => Set(value);
        }

        public bool IsChecked
        {
            get => Get<bool>();
            set => Set(value);

        }

        protected virtual void DoNotifyLayoutChanged()
        {
            
        }
    }
}
