﻿namespace ChyronHego.Enterprise.Presentation.Layout
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using ChyronHego.Enterprise.Presentation.Extensions;
    using ChyronHego.Enterprise.Presentation.Interfaces;
    using WinFormsControl = System.Windows.Forms.Control;
    using WinFormsUserControl = System.Windows.Forms.UserControl;

    public class AvalonDockChildWindowViewModel : LayoutContentViewModel
    {
        private UserControl userControl;

        public AvalonDockChildWindowViewModel(ILayoutContentControl childControl)
        {
            ChildContentControl = childControl;
            Title = childControl?.Title;
            ContentId = childControl?.ContentId;

            if (childControl is WinFormsUserControl userControl)
            {
                AttachKeyPressEventForwarding(userControl);
            }
        }

        public ILayoutContentControl ChildContentControl
        {
            get => Get<ILayoutContentControl>();
            set => Set(value);
        }

        public string ToolTip
        {
            get => Get<string>();
            set => Set(value);
        }

        public virtual string IconSource { get; } = string.Empty;

        public UserControl UserControl
        {
            get
            {
                if (userControl != null)
                {
                    return userControl;
                }

                if (!(ChildContentControl?.AsWpfUserControl() is UserControl newUserControl))
                {
                    return null;
                }

                userControl = newUserControl;

                userControl.SizeChanged += (sender, args) =>
                {
                    DoNotifyLayoutChanged();
                };

                return userControl;
            }
        }

        private void AttachKeyPressEventForwarding(WinFormsControl control)
        {
            if (control == null)
            {
                return;
            }

            foreach (WinFormsControl childControl in control.Controls)
            {
                AttachKeyPressEventForwarding(childControl);
            }

            control.KeyDown += WinFormsControl_KeyDown;
        }

        private void WinFormsControl_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (UserControl == null)
            {
                return;
            }

            if (!(Window.GetWindow(UserControl) is Window parentWindow))
            {
                return;
            }

            if (!(PresentationSource.FromVisual(parentWindow) is PresentationSource presentationSource))
            {
                return;
            }

            var key = KeyInterop.KeyFromVirtualKey(e.KeyValue);

            var keyArgs = new KeyEventArgs(
                Keyboard.PrimaryDevice,
                presentationSource,
                0,
                key)
            {
                RoutedEvent = UIElement.KeyDownEvent, 
                Source = sender
            };

            parentWindow.RaiseEvent(keyArgs);
        }
    }
}
