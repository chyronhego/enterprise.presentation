﻿namespace ChyronHego.Enterprise.Presentation.Layout
{
    using AvalonDock;

    public interface ILayoutSerializerFactory
    {
        ILayoutSerializer GetLayoutSerializer(string layoutName, DockingManager dockingManager);
    }
}
