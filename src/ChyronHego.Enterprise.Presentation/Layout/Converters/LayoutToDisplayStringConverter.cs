﻿namespace ChyronHego.Enterprise.Presentation.Layout.Converters
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Windows.Data;
    using ChyronHego.Enterprise.Presentation.Converters;

    public class LayoutToDisplayStringConverter : BaseConverter, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string stringValue)
            {
                return DirectConvert(stringValue);
            }

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        public string DirectConvert(string layoutFilePath)
        {
            try
            {
                return Path.GetFileNameWithoutExtension(layoutFilePath);
            }
            catch
            {
            }

            return string.Empty;
        }
    }
}
