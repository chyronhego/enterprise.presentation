﻿namespace ChyronHego.Enterprise.Presentation.Layout
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Windows.Threading;
    using ChyronHego.Enterprise.Logging;

    public class LayoutChangeManager
    {
        private static LayoutChangeManager layoutChangeManager;

        public static LayoutChangeManager Instance =>
            layoutChangeManager ?? (layoutChangeManager = new LayoutChangeManager());

        public event Action<LayoutType> LayoutChanged;

        private readonly Dispatcher dispatcher;

        private readonly Thread thread;

        private readonly object changeSetLock = new object();

        private readonly HashSet<LayoutType> changeSet = new HashSet<LayoutType>();

        private ReferenceCountFlag suspendNotificationsFlag = new ReferenceCountFlag();

        public LayoutChangeManager()
        {
            dispatcher = Dispatcher.CurrentDispatcher;

            thread = new Thread(NotifyThread)
            {
                Name = "Layout Change Notification Thread",
                IsBackground = true
            };

            thread.Start();
        }

        private void DoNotify(LayoutType layoutType)
        {
            if (!dispatcher.CheckAccess())
            {
                dispatcher.BeginInvoke(new Action<LayoutType>(DoNotify), layoutType);
                return;
            }
            LayoutChanged?.Invoke(layoutType);
        }

        private void WaitForIncomingNotifications()
        {
            lock (changeSetLock)
            {
                if (changeSet.Count == 0)
                {
                    Monitor.Wait(changeSetLock);
                }
            }
        }

        private void WaitForThrottlingTimeout(TimeSpan timeSpan)
        {
            Thread.Sleep(timeSpan);
        }

        private bool CanNotify => !suspendNotificationsFlag.IsSet;

        private void ProcessPendingNotifications()
        {
            if (!CanNotify)
            {
                return;
            }

            var layoutTypeList = new List<LayoutType>();

            lock (changeSetLock)
            {
                layoutTypeList.AddRange(changeSet);
                if (!EnabledNotificationThrottling)
                {
                    changeSet.Clear();
                }
            }

            foreach (var changedLayoutType in layoutTypeList)
            {
                DoNotify(changedLayoutType);
            }

            if (EnabledNotificationThrottling)
            {
                WaitForThrottlingTimeout(TimeSpan.FromSeconds(1));

                lock (changeSetLock)
                {
                    foreach (var changedLayoutType in layoutTypeList)
                    {
                        changeSet.Remove(changedLayoutType);
                    }
                }
            }
        }

        private void NotifyThread()
        {
            while (true)
            {
                try
                {
                    WaitForIncomingNotifications();

                    ProcessPendingNotifications();
                }
                catch (Exception e)
                {
                    LogManager.Log(nameof(LayoutChangeManager), nameof(NotifyThread), e);
                }
            }
        }

        public bool EnabledNotificationThrottling { get; set; } = true;

        public void NotifyLayoutChanged(LayoutType layoutType)
        {
            if (!CanNotify)
            {
                return;
            }

            lock (changeSetLock)
            {
                changeSet.Add(layoutType);

                Monitor.Pulse(changeSetLock);
            }
        }

        public void SuspendNotifications()
        {
            suspendNotificationsFlag.SetFlag();
        }

        public void ResumeNotifications()
        {
            suspendNotificationsFlag.ClearFlag();
        }
    }
}
