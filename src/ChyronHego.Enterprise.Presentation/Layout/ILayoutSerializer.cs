﻿namespace ChyronHego.Enterprise.Presentation.Layout
{
    using System;
    using AvalonDock.Layout.Serialization;

    public interface ILayoutSerializer
    {
        void Serialize();

        void Deserialize();

        event EventHandler<LayoutSerializationCallbackEventArgs> LayoutSerializationCallback;
    }
}
