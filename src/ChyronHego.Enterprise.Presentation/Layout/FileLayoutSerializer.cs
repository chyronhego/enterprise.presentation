﻿namespace ChyronHego.Enterprise.Presentation.Layout
{
    using System.IO;
    using System.Xml.Serialization;
    using AvalonDock;
    using AvalonDock.Layout;

    public class FileLayoutSerializer : BaseLayoutSerializer
    {
        public FileLayoutSerializer(string filePath, DockingManager dockingManager) : base(dockingManager)
        {
            FilePath = filePath;
        }

        public string FilePath { get; }

        protected override void SerializeInternal(LayoutRoot layout)
        {
            if (layout == null || string.IsNullOrWhiteSpace(FilePath))
            {
                return;
            }

            using (var fileStream = new FileStream(FilePath, FileMode.Create))
            {
                var serializer = new XmlSerializer(typeof(LayoutRoot));
                serializer.Serialize(fileStream, layout);
            }
        }

        protected override LayoutRoot DeserializeInternal()
        {
            if (!File.Exists(FilePath))
            {
                return null;
            }

            using (var fileStream = new FileStream(FilePath, FileMode.Open))
            {
                var serializer = new XmlSerializer(typeof(LayoutRoot));
                var layout = serializer.Deserialize(fileStream) as LayoutRoot;
                return layout;
            }
        }
    }
}
