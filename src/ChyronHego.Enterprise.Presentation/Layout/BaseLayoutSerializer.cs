﻿namespace ChyronHego.Enterprise.Presentation.Layout
{
    using System;
    using System.Linq;
    using System.Windows;
    using AvalonDock;
    using AvalonDock.Layout;
    using AvalonDock.Layout.Serialization;
    using ChyronHego.Enterprise.Presentation.Interfaces;

    public class BaseLayoutSerializer : LayoutSerializer, ILayoutSerializer
    {
        public event Action BeforeSerialization;
        public event Action AfterSerialization;
        public event Action BeforeDeserialization;
        public event Action AfterDeserialization;
        public event Action<LayoutRoot> RequestFixupLayout;

        public BaseLayoutSerializer(DockingManager dockingManager) 
            : base(dockingManager)
        {
            
        }

        public virtual void Serialize()
        {
            BeforeSerialization?.Invoke();

            SerializeInternal(Manager?.Layout);

            AfterSerialization?.Invoke();
        }

        public virtual void Deserialize()
        {
            try
            {
                StartDeserialization();
                BeforeDeserialization?.Invoke();

                var layout = DeserializeInternal();

                if (layout == null)
                {
                    return;
                }

                FixupLayout(layout);
                Manager.Layout = layout;
            }
            finally
            {
                AfterDeserialization?.Invoke();
                EndDeserialization();
            }
        }

        protected virtual void PrepareContentBeforeDeserialization(object Content)
        {
            if (Content is ILayoutContentViewModel viewModel)
            {
                viewModel.IsVisible = false;
                viewModel.IsActive = false;
                viewModel.IsSelected = false;
            }
        }

        protected virtual void PrepareChildContent()
        {
            if (Manager == null)
            {
                return;
            }

            foreach (var anchorable in Manager.Layout.Descendents().OfType<LayoutAnchorable>().ToArray())
            {
                PrepareContentBeforeDeserialization(anchorable.Content);
            }

            foreach (var document in Manager.Layout.Descendents().OfType<LayoutDocument>().ToArray())
            {
                PrepareContentBeforeDeserialization(document.Content);
            }
        }

        protected override void FixupLayout(LayoutRoot layout)
        {
            var previousVisibility = Manager.Visibility;
            try
            {
                Manager.Visibility = Visibility.Hidden;
                PrepareChildContent();
                RequestFixupLayout?.Invoke(layout);
                base.FixupLayout(layout);
            }
            finally
            {
                Manager.Visibility = previousVisibility;
            }
        }

        protected virtual void SerializeInternal(LayoutRoot layout)
        {

        }

        protected virtual LayoutRoot DeserializeInternal()
        {
            return null;
        }
    }
}
