﻿namespace ChyronHego.Enterprise.Presentation.Layout
{
    using System;
    using System.Threading;

    public class ReferenceCountFlag
    {
        private long refCount = 0;

        public bool SetFlag()
        {
            return Interlocked.Increment(ref refCount) > 0;
        }

        public bool ClearFlag()
        {
            return Interlocked.Decrement(ref refCount) > 0;
        }

        public bool IsSet => Interlocked.Read(ref refCount) > 0;

        public IDisposable Start()
        {
            return new ReferenceRefCountFlagHelper(this);
        }
    }

    public class ReferenceRefCountFlagHelper : IDisposable
    {
        public ReferenceRefCountFlagHelper(ReferenceCountFlag parent)
        {
            Parent = parent ?? throw new ArgumentNullException(nameof(parent));
            Parent.SetFlag();
        }

        public ReferenceCountFlag Parent { get; }

        public void Dispose()
        {
            Parent.ClearFlag();
        }
    }
}
