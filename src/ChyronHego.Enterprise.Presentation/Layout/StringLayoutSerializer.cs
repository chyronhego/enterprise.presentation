﻿namespace ChyronHego.Enterprise.Presentation.Layout
{
    using AvalonDock;
    using AvalonDock.Layout;
    using Enterprise.Infrastructure.Extensions;
    using System.IO;
    using System.Xml.Serialization;

    public class StringLayoutSerializer : BaseLayoutSerializer
    {
        public StringLayoutSerializer(DockingManager dockingManager) : base(dockingManager)
        {
        }

        public string Value { get; set; }

        protected override void SerializeInternal(LayoutRoot layout)
        {
            if (layout == null)
            {
                return;
            }

            Value = string.Empty;

            using (var stream = new StringWriter())
            {
                var serializer = new XmlSerializer(typeof(LayoutRoot));
                serializer.Serialize(stream, layout);
                Value = stream.ToString();
            }
        }

        protected override LayoutRoot DeserializeInternal()
        {
            if (Value.IsNullOrEmpty())
            {
                return null;
            }

            using (var stringStream = new StringReader(Value))
            {
                var serializer = new XmlSerializer(typeof(LayoutRoot));
                var layout = serializer.Deserialize(stringStream) as LayoutRoot;
                return layout;
            }
        }
    }
}
