﻿namespace ChyronHego.Enterprise.Presentation.Models.Bindings
{
    using System;
    using System.ComponentModel;

    public class NotifyBinding
    {
        private INotifyPropertyChanged source;

        protected Action updateDestination;

        public NotifyBinding(string sourceProperty)
        {
            SourceProperty = sourceProperty;
        }

        public NotifyBinding(string sourceProperty, Action updateDestination)
            : this(sourceProperty)
        {
            this.updateDestination = updateDestination;
        }

        public string SourceProperty { get; }

        public INotifyPropertyChanged Source
        {
            get => source;
            set
            {
                if (source != null)
                {
                    source.PropertyChanged -= Source_PropertyChanged;
                }

                source = value;

                if (source != null)
                {
                    OnUpdateDestination();

                    source.PropertyChanged += Source_PropertyChanged;
                }
            }
        }

        protected virtual void OnUpdateDestination()
        {
            updateDestination?.Invoke();
        }

        private void Source_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == SourceProperty)
            {
                OnUpdateDestination();
            }
        }
    }
}
