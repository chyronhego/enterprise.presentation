﻿namespace ChyronHego.Enterprise.Presentation.Models.Bindings
{
    using System.Windows;
    using ChyronHego.Enterprise.Presentation.Models;

    public class NotifyPropertyBinding : NotifyBinding
    {
        private NotifyModel source;

        public NotifyPropertyBinding(string sourceProperty, DependencyProperty destinationProperty, DependencyObject destination)
            : base(sourceProperty)
        {
            DestinationProperty = destinationProperty;

            Destination = destination;
        }

        public DependencyProperty DestinationProperty { get; set; }

        public DependencyObject Destination { get; set; }

        public new NotifyModel Source
        {
            get => source;
            set
            {
                source = value;

                base.Source = source;
            }
        }

        protected override void OnUpdateDestination()
        {
            base.OnUpdateDestination();

            object value = source.GetValue(SourceProperty);

            Destination.SetValue(DestinationProperty, value);
        }
    }
}
