﻿namespace ChyronHego.Enterprise.Presentation.Models
{
    using System.Collections.Generic;
    using System.ComponentModel;

    public interface INotifyTable : INotifyPropertyChanged
    {
        IEnumerable<KeyValuePair<string, object>> Properties { get; }

        object GetValue(string property);

        T GetValue<T>(string property);

        bool TryGetValue(string property, out object value);

        void SetValue(string property, object value);
    }
}
