﻿namespace ChyronHego.Enterprise.Presentation.Models
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public class NotifyModel : INotifyTable
    {
        protected readonly Dictionary<string, object> table = new Dictionary<string, object>();

        protected event PropertyChangedEventHandler propertyChanged;

        public event PropertyChangedEventHandler PropertyChanged
        {
            add { OnAttachPropertyChangedHandler(value); }
            remove { OnDetachPropertyChangedHandler(value); }
        }

        protected bool IsPropertyChangedEmpty => propertyChanged == null;

        protected PropertyChangedEventHandler PropertyChangedHandler => propertyChanged;

        public IEnumerable<KeyValuePair<string, object>> Properties => table;

        public IEnumerable<string> PropertyNames => table.Keys;

        protected virtual void OnAttachPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            propertyChanged += handler;
        }

        protected virtual void OnDetachPropertyChangedHandler(PropertyChangedEventHandler handler)
        {
            propertyChanged -= handler;
        }

        protected T Get<T>([CallerMemberName] string property = "")
        {
            if (!TryGetValue(property, out object value))
            {
                value = default(T);
            }

            return (T)value;
        }

        protected virtual bool TrySetValue(object value, [CallerMemberName] string property = "")
        {
            if (!table.TryGetValue(property, out object previousValue) ||
                value == null || !value.Equals(previousValue))
            {
                table[property] = value;

                OnPropertyChanged(property);

                return true;
            }

            return false;
        }

        protected virtual void Set(object value, [CallerMemberName] string property = "")
        {
            TrySetValue(value, property);
        }

        protected virtual void OnPropertyChanged([CallerMemberName]string property = "")
        {
            propertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        public virtual bool TryGetValue(string property, out object value)
        {
            return table.TryGetValue(property, out value);
        }

        public object GetValue(string property)
        {
            return Get<object>(property);
        }

        public T GetValue<T>(string property)
        {
            return Get<T>(property);
        }

        public void SetValue(string property, object value)
        {
            Set(value, property);
        }
    }
}
