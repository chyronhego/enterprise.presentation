﻿namespace ChyronHego.Enterprise.Presentation.Styles
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using ChyronHego.Enterprise.Presentation.Styles.Properties;

    /// <summary>
    /// This class is used to allow the programmer to create a StyleSelector on-the-fly within XAML
    /// without the need to build a new selector class for each possible scenario where the programmer
    /// wants to set a style based on the type of the item within a xaml collection, originally
    /// developed to aide with styling docking panel controls based on their view model type
    /// 
    /// usage example:
    /// <SomeClassItemStyleSelector>
    ///     <DynamicStyleSelector>
    ///         <DynamicStyleSelector.Styles>
    ///             <DynamicStyle DataType="{x:Type typeOfItemInTheCollection}">
    ///                 <DynamicStyle.Style>
    ///                     <Style TargetType="{x:Type typeToWhichStyleIsActuallyApplied}">
    ///                         <Setter Property = "Title" Value="{Binding Model.Title}"/>
    ///                         <Setter Property = "IconSource" Value="{Binding Model.IconSource}"/>
    ///                         <Setter Property = "Visibility" Value="{Binding Model.IsVisible, Mode=TwoWay, Converter={StaticResource BoolToVisibilityConverter}}"/>
    ///                     </Style>
    ///                 </DynamicStyle.Style>
    ///             </DynamicStyle>
    ///         </DynamicStyleSelector.Styles>
    ///     </DynamicStyleSelector>
    /// </SomeClassItemStyleSelector>
    ///
    ///
    /// </summary>
    public class DynamicStyleSelector : StyleSelector
    {
        private bool hasBeenSorted = false;

        private void SortStylesByInheritance()
        {
            if (hasBeenSorted)
            {
                return;
            }

            lock (this)
            {
                if (hasBeenSorted)
                {
                    return;
                }

                hasBeenSorted = true;
            }
            //sort the styles from descendant up to ancestor, this way if the programmer
            //includes different styles for a descendant and an ancestor, the style for 
            //the lowest level descendant will be returned first
            List<DynamicStyle> temp = new List<DynamicStyle>(Styles);
            temp.Sort((x, y) =>
            {
                if (x.DataType == y.DataType)
                {
                    return 0;
                }

                if (x.DataType == null)
                {
                    return 1;
                }

                if (y.DataType == null)
                {
                    return -1;
                }

                if (x.DataType.IsSubclassOf(y.DataType))
                {
                    return -1;
                }

                return y.DataType.IsSubclassOf(x.DataType) ? 1 : 0;
            });
        }

        public List<DynamicStyle> Styles { get; } = new List<DynamicStyle>();

        public override Style SelectStyle(object item, DependencyObject container)
        {
            SortStylesByInheritance();

            if (item == null)
            {
                return base.SelectStyle(item, container);
            }

            return Styles.FirstOrDefault(s => s.DataType?.IsInstanceOfType(item) == true)?.Style ??
                   base.SelectStyle(item, container);
        }
    }
}
