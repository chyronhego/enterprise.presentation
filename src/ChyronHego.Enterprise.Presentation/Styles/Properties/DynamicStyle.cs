﻿namespace ChyronHego.Enterprise.Presentation.Styles.Properties
{
    using System;
    using System.Windows;

    /// <summary>
    /// Used in conjunction with the DynamicStyleSelector, this allows the user
    /// to specify a custom selector in XAML for a given DataType without the
    /// need to build a new selector class for each different usage scenario required
    /// </summary>
    public class DynamicStyle : DependencyObject
    {
        /// <summary>
        /// Provides the value used to match this <see cref="DynamicStyle"/> to an item
        /// </summary>
        public static readonly DependencyProperty DataTypeProperty =
            DependencyProperty.Register("DataType", typeof(Type), typeof(DynamicStyle));

        /// <summary>
        /// Provides the <see cref="DynamicStyle"/> used to render items matching the <see cref="DataType"/>
        /// </summary>
        public static readonly DependencyProperty DynamicStyleProperty =
            DependencyProperty.Register("Style", typeof(Style), typeof(DynamicStyle));

        /// <summary>
        /// Gets or Sets the value used to match this <see cref="DynamicStyle"/> to an item
        /// </summary>
        public Type DataType
        {
            get => (Type)GetValue(DataTypeProperty);
            set => SetValue(DataTypeProperty, value);
        }

        /// <summary>
        /// Gets or Sets the <see cref="DynamicStyle"/> used to render items matching the <see cref="DataType"/>
        /// </summary>
        public Style Style
        {
            get => (Style)GetValue(DynamicStyleProperty);
            set => SetValue(DynamicStyleProperty, value);
        }
    }
}
