﻿namespace ChyronHego.Enterprise.Presentation.Styles.Properties
{
    using System.Windows;
    using System.Windows.Media;
    using ChyronHego.Enterprise.Presentation.Extensions;
    using ChyronHego.Enterprise.Presentation.Views;

    public class ListViewStyleManager : DependencyObject
    {
        public static readonly DependencyProperty BackColorProperty =
           DependencyProperty.Register("BackColor", typeof(Color), typeof(ListViewStyleManager), new FrameworkPropertyMetadata());

        public static readonly DependencyProperty ForeColorProperty =
           DependencyProperty.Register("ForeColor", typeof(Color), typeof(ListViewStyleManager), new FrameworkPropertyMetadata());

        public static readonly DependencyProperty SelectionColorProperty =
           DependencyProperty.Register("SelectionColor", typeof(Color), typeof(ListViewStyleManager), new FrameworkPropertyMetadata());

        public static readonly DependencyProperty SelectionTextColorProperty =
           DependencyProperty.Register("SelectionTextColor", typeof(Color), typeof(ListViewStyleManager), new FrameworkPropertyMetadata());

        private const double HoverDarkCoefficient = 0.6;

        private const double HoverSelectedInactiveDarkCoefficient = 0.8;

        private const double SelectedDarkCoefficient = 0.9;

        private BaseView baseView;

        public ListViewStyleManager(BaseView baseView)
        {
            this.baseView = baseView;
        }

        public Color BackColor
        {
            get { return (Color)GetValue(BackColorProperty); }
            set
            {
                SetCurrentValue(BackColorProperty, value);
            }
        }

        public Color ForeColor
        {
            get { return (Color)GetValue(ForeColorProperty); }
            set
            {
                SetCurrentValue(ForeColorProperty, value);
            }
        }

        public Color SelectionColor
        {
            get { return (Color)GetValue(SelectionColorProperty); }
            set
            {
                SetCurrentValue(SelectionColorProperty, value);
            }
        }

        public Color SelectionTextColor
        {
            get { return (Color)GetValue(SelectionTextColorProperty); }
            set
            {
                SetCurrentValue(SelectionTextColorProperty, value);
            }
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            SetResourceProperty(e.Property, e.NewValue);
        }

        private void ApplySelectionColorBasedTheme(Color color)
        {
            SetResourceValue("ListItemSelectedHover", color);
            SetResourceValue("ListItemHover", color.GetColorDarker(HoverDarkCoefficient));
            SetResourceValue("ListItemSelected", color.GetColorDarker(SelectedDarkCoefficient));
            SetResourceValue("ListItemSelectedInactive", color.GetColorDarker(HoverSelectedInactiveDarkCoefficient));
        }

        private void SetResourceProperty(DependencyProperty property, object value)
        {
            if (property == BackColorProperty)
            {
                SetResourceValue("Window", value);
            }
            else if (property == ForeColorProperty)
            {
                SetResourceValue("WindowText", value);
            }
            else if (property == SelectionColorProperty)
            {
                ApplySelectionColorBasedTheme((Color)value);
            }
            else if (property == SelectionTextColorProperty)
            {
                SetResourceValue("HighlightText", value);
            }
        }

        private void SetResourceValue(string resourceKey, object value)
        {
            if (baseView.Resources.Contains(resourceKey))
            {
                baseView.Resources[resourceKey] = value;
            }
        }
    }
}