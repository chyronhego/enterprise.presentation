﻿namespace ChyronHego.Enterprise.Presentation.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;

    interface IRelayCommand : ICommand
    {
    }

    interface IRelayCommand<T> : IRelayCommand
    {
        bool CanExecute(T parameter);
        void Execute(T parameter);
    }
}
