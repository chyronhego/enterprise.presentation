﻿namespace ChyronHego.Enterprise.Presentation.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    
    //
    // Summary:
    //     A command designed to relay its functionality to other objects by invoking
    //     delegates.  CanExecute returns 'true' by default.  Non-generic RelayCommand
    //     does not allow you to accept command parameters for Execute and CanExecute,
    //     the generic RelayCommand does allow you to accept command parameters of the
    //     type T
    //
    public class RelayCommand : IRelayCommand
    {
        #region Fields
        private Action doExecute = null;
        private Func<bool> canExecute = null;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Create a non-generic relay command
        /// </summary>
        protected RelayCommand()
        {

        }

        /// <summary>
        /// Create a non-generic relay command
        /// </summary>
        /// <param name="DoExecute">The execution logic.</param>
        public RelayCommand(Action DoExecute) : this(DoExecute, null)
        {

        }

        /// <summary>
        /// Create a non-generic relay command
        /// </summary>
        /// <param name="DoExecute">The execution logic.</param>
        /// <param name="CanExecute">The execution status logic.</param>
        public RelayCommand(Action DoExecute, Func<bool> CanExecute)
        {
            if (DoExecute == null)
            {
                throw new ArgumentNullException("DoExecute");
            }
            doExecute = DoExecute;
            canExecute = CanExecute;
        }
        #endregion Constructors

        #region IRelayCommand/ICommand members
        /// <summary>
        /// Event to fire whenever the status of CanExecute has changed.  This lets
        /// subscribers know they must re-query CanExecute in order to enable/disable
        /// bound controls and properties
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// non-generic implementation of ICommand.CanExecute
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns>True if the command can execute</returns>
        public virtual bool CanExecute(object parameter)
        {
            return canExecute?.Invoke() ?? true;
        }

        /// <summary>
        /// non-generic implementation of ICommand.Execute
        /// </summary>
        /// <param name="parameter"></param>
        public virtual void Execute(object parameter)
        {
            doExecute?.Invoke();
        }

        /// <summary>
        /// Explicitly inform subscribers of CanExecuteChanged of a change.
        /// </summary>
        public virtual void RaiseCanExecuteChanged()
        {
            CommandManager.InvalidateRequerySuggested();
        }
        #endregion IRelayCommand/ICommand members
    }

    public class RelayCommand<T> : RelayCommand, IRelayCommand<T>
    {
        #region Fields

        private Action<T> doExecute = null;
        private Func<T, bool> canExecute = null;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Creates a new RelayCommand.
        /// </summary>
        /// <param name="DoExecute">The execution logic.</param>
        public RelayCommand(Action<T> DoExecute) : this(DoExecute, null)
        {

        }

        /// <summary>
        /// Creates a new RelayCommand.
        /// </summary>
        /// <param name="DoExecute">The execution logic.</param>
        /// <param name="CanExecute">The execution status logic.</param>
        public RelayCommand(Action<T> DoExecute, Func<T, bool> CanExecute)
        {
            if (DoExecute == null)
            {
                throw new ArgumentNullException("DoExecute");
            }
            doExecute = DoExecute;
            canExecute = CanExecute;
        }

        #endregion Constructors

        #region IRelayCommand<T> Members

        /// <summary>
        /// Non-generic implementation of ICommand.CanExecute
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns>True if the command can execute</returns>
        public override bool CanExecute(object parameter)
        {
            return CanExecute((T)parameter);
        }

        /// <summary>
        /// Generic implementation of IRelayCommand<T>.CanExecute
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns>True if the command can execute</returns>
        public bool CanExecute(T parameter)
        {
            return canExecute?.Invoke(parameter) ?? true;
        }

        /// <summary>
        /// Non-Generic implementation of ICommand.Execute
        /// </summary>
        /// <param name="parameter"></param>
        public override void Execute(object parameter)
        {
            Execute((T)parameter);
        }

        /// <summary>
        /// Generic implmentation of IRelayCommand<T>.Execute
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(T parameter)
        {
            doExecute?.Invoke(parameter);
        }

        #endregion IRelayCommand<T> Members
    }
}
