﻿namespace ChyronHego.Enterprise.Presentation.Interfaces
{
    public interface ITitle
    {
        string Title { get; }
    }
}
