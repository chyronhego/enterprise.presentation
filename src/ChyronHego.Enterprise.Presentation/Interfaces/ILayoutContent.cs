﻿namespace ChyronHego.Enterprise.Presentation.Interfaces
{
    public interface ILayoutContent : ITitle
    {
        string ContentId { get; }
    }
}
