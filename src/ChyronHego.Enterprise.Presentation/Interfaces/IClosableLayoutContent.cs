﻿namespace ChyronHego.Enterprise.Presentation.Interfaces
{
    using System;

    public interface IClosableLayoutContent : ILayoutContentControl
    {
        event EventHandler Closing;
    }
}
