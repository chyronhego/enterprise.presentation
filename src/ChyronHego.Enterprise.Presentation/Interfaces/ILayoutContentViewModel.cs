﻿namespace ChyronHego.Enterprise.Presentation.Interfaces
{
    public interface ILayoutContentViewModel : ILayoutContent
    {
        bool IsSelected { get; set; }

        bool IsVisible { get; set; }

        bool IsActive { get; set; }
    }
}
