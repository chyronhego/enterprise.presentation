﻿namespace ChyronHego.Enterprise.Presentation.Interfaces
{
    public interface ILayoutContentControl : IUserControl, ILayoutContent
    {
        
    }
}
