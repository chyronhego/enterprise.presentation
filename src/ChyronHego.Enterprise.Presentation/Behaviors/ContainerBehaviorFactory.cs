﻿namespace ChyronHego.Enterprise.Presentation.Behaviors
{
    using System.Windows;
    using System.Windows.Controls;
    using ChyronHego.Enterprise.Presentation.Behaviors.DragDrop;

    public static class ContainerBehaviorFactory
    {
        public static T CreateBehaviorStrategy<T>(IFrameworkInputElement element, BehaviorTypes type) where T : class, IContainerBehavior
        {
            return type switch
            {
                BehaviorTypes.Drag when element is DataGrid => new GridContainerDragBehavior(element) as T,
                BehaviorTypes.Drag => new DefaultContainerDragBehavior(element) as T,
                BehaviorTypes.Drop => new DefaultContainerDropBehavior(element) as T,
                _ => null
            };
        }
    }
}
