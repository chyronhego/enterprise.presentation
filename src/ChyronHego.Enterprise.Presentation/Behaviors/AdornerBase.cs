﻿namespace ChyronHego.Enterprise.Presentation.Behaviors
{
    using System.Windows;
    using System.Windows.Documents;

    public abstract class AdornerBase : Adorner
    {
        private readonly AdornerLayer adornerLayer;

        protected AdornerBase(UIElement adornedElement) : base(adornedElement)
        {
            adornerLayer = AdornerLayer.GetAdornerLayer(adornedElement);
            adornerLayer?.Add(this);
        }

        public virtual AdornerLayer AdornerLayer => adornerLayer;

        public virtual void Clear()
        {
            var adorners = adornerLayer.GetAdorners(AdornedElement);

            if (adorners != null)
            {
                foreach (var adorner in adorners)
                {
                    adornerLayer.Remove(adorner);
                }
            }
        }

        public virtual void Update()
        {
            if (adornerLayer.GetAdorners(AdornedElement) != null)
            {
                adornerLayer.Update(AdornedElement);
                adornerLayer.Visibility = Visibility.Visible;
            }
            else
            {
                adornerLayer.Add(this);
                adornerLayer.Update(AdornedElement);
            }
        }

        public virtual void Hide()
        {
            adornerLayer.Visibility = Visibility.Hidden;
        }
    }
}
