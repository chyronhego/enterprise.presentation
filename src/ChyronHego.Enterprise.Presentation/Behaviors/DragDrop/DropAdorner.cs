﻿namespace ChyronHego.Enterprise.Presentation.Behaviors.DragDrop
{
    using System.Windows;
    using System.Windows.Media;

    public class DropAdorner : AdornerBase
    {
        private DropLocationType targetLocation;

        public DropAdorner(UIElement adornedElement) : base(adornedElement)
        {
        }

        internal void Update(DropLocationType dropLocation)
        {
            targetLocation = dropLocation;

            base.Update();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            var size = new Size(AdornedElement.RenderSize.Width, 1.5);

            double height = 5.0;
            double width = 5.0;

            var center = new Point(0, 0);

            center.Y += targetLocation == DropLocationType.Below ? AdornedElement.RenderSize.Height : 0; 

            var rect = new Rect(center, size);
            var brush = new SolidColorBrush(Colors.Gray);
            var pen = new Pen(brush, 1.0);

            drawingContext.DrawRectangle(brush, pen, rect);
            drawingContext.DrawRectangle(brush, pen, new Rect(new Point(center.X, center.Y - height/2), new Size(width,height)));
            drawingContext.DrawRectangle(brush, pen, 
                new Rect(new Point(center.X + size.Width - width, center.Y - height/2), new Size(width, height)));
        }
    }
}
