﻿namespace ChyronHego.Enterprise.Presentation.Behaviors.DragDrop
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using ChyronHego.Enterprise.Presentation.Controls;

    public class DefaultContainerDropBehavior : IContainerDropBehavior
    {
        private IDroppable dropControlViewModel;

        private UIElement droppedOverElement;

        private IDraggable sourceObject;

        private int dropIndex;

        private DropType dropType = DropType.Reorder;

        private DropLocationType dropLocation = DropLocationType.Below;

        private IEnumerable<Type> supportedTypes;

        private bool isDragInProgress = false;

        private DropAdorner adorner;

        private readonly FrameworkElement associatedObject;

        public DefaultContainerDropBehavior(IFrameworkInputElement element)
        {
            associatedObject = element as FrameworkElement;
        }

        public void OnAttached()
        {
            if (associatedObject == null)
            {
                return;
            }

            associatedObject.DragEnter += OnDragEnter;
            associatedObject.DragLeave += OnDragLeave;
            associatedObject.DragOver += OnDragOver;
            associatedObject.Drop += OnDragDrop;
        }

        public void OnDragEnter(object sender, DragEventArgs e)
        {
            if (isDragInProgress && sourceObject?.GetParent() != associatedObject.DataContext)
            {
                dropType = DropType.Insert;
                supportedTypes = null;
            }

            if (supportedTypes == null)
            {
                dropControlViewModel = associatedObject.DataContext as IDroppable;

                supportedTypes = dropControlViewModel?.GetSupportedDataTypes();

                if (supportedTypes == null)
                {
                    return;
                }
            }

            foreach (var type in supportedTypes)
            {
                if (e.Data.GetDataPresent(type))
                {
                    sourceObject = e.Data.GetData(type) as IDraggable;
                }
            }

            e.Handled = true;
        }

        public void OnDragOver(object sender, DragEventArgs e)
        {
            isDragInProgress = true;

            if (supportedTypes != null)
            {
                foreach (var type in supportedTypes)
                {
                    if (e.Data.GetDataPresent(type))
                    {
                        SetDragDropEffects(e);

                        var container = sender as ItemsControl;

                        var currentElement = UIHelper.GetUIElement(container, e.GetPosition(container));

                        if (currentElement != null && currentElement != droppedOverElement)
                        {
                            adorner = new DropAdorner(currentElement);
                        }

                        droppedOverElement = currentElement;

                        if (droppedOverElement != null)
                        {
                            bool isAboveItem = UIHelper.IsPositionAboveElement(droppedOverElement, e.GetPosition(droppedOverElement));

                            dropLocation = isAboveItem ? DropLocationType.Above : DropLocationType.Below;
                        }

                        adorner?.Update(dropLocation);
                    }

                    SetDragDropEffects(e);
                }
            }

            e.Handled = true;
        }

        public void OnDragLeave(object sender, DragEventArgs e)
        {
            adorner?.Clear();

            e.Handled = true;
        }

        public void OnDragDrop(object sender, DragEventArgs e)
        {
            foreach (var type in supportedTypes)
            {
                if (e.Data.GetDataPresent(type))
                {
                    if (!(sender is ItemsControl containerTarget))
                    {
                        return;
                    }

                    dropIndex = containerTarget.ItemContainerGenerator.IndexFromContainer(droppedOverElement);

                    if (dropType != DropType.Insert)
                    {
                        sourceObject?.Remove(e.Data.GetData(type));
                    }

                    if (dropLocation == DropLocationType.Below)
                    {
                        dropIndex++;
                    }

                    dropControlViewModel.Drop(sourceObject, dropIndex);

                    dropType = DropType.Reorder;
                    e.Handled = true;
                }
            }

            isDragInProgress = false;

            adorner?.Hide();
        }

        private void SetDragDropEffects(DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;

            foreach (var type in supportedTypes)
            {
                if (e.Data.GetDataPresent(type))
                {
                    e.Effects = DragDropEffects.Move;
                }
            }
        }
    }
}
