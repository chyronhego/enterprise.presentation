﻿namespace ChyronHego.Enterprise.Presentation.Behaviors.DragDrop
{
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Interactivity;
    
    public class DragBehavior : Behavior<FrameworkElement>
    {
        private bool isMouseClicked = false;

        private readonly IContainerDragBehavior containerBehavior;

        public DragBehavior()
        {
            containerBehavior = ContainerBehaviorFactory.CreateBehaviorStrategy<IContainerDragBehavior>(AssociatedObject, BehaviorTypes.Drag);
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            containerBehavior.OnAttached();
            containerBehavior.MouseLeave += AssociatedObject_MouseLeave;
            containerBehavior.MouseButtonDown += AssociatedObject_MouseLeftButtonDown;
            containerBehavior.MouseButtonUp += AssociatedObject_MouseLeftButtonUp;
        }

        protected virtual void AssociatedObject_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            isMouseClicked = true;
        }

        protected virtual void AssociatedObject_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            isMouseClicked = false;
        }

        protected virtual void AssociatedObject_MouseLeave(object sender, MouseEventArgs e)
        {
            if (isMouseClicked)
            {
                var element = sender as FrameworkElement;
                
                if (element?.DataContext is IDraggable dragObject)
                {
                    var data = new DataObject();
                    data.SetData(dragObject.DataType, element.DataContext);

                    System.Windows.DragDrop.DoDragDrop(AssociatedObject, data, dragObject.Effects);
                }
            }

            isMouseClicked = false;
        }
    }
}
