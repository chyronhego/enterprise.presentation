﻿namespace ChyronHego.Enterprise.Presentation.Behaviors.DragDrop
{
    using System;
    using System.Windows;

    public interface IDraggable
    {
        Type DataType { get; }

        DragDropEffects Effects { get; set; }

        void Remove(object i);

        object GetParent();

        object GetData();

        string GetName();
    }
}
