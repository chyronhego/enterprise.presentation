﻿namespace ChyronHego.Enterprise.Presentation.Behaviors.DragDrop
{
    using System;
    using System.Collections.Generic;
    
    public interface IDroppable
    {
        void Drop(object data, int index = -1);

        IEnumerable<Type> GetSupportedDataTypes();
    }
}
