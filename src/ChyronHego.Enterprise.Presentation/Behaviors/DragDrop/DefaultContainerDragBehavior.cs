﻿namespace ChyronHego.Enterprise.Presentation.Behaviors.DragDrop
{
    using System.Windows;
    using System.Windows.Input;

    public class DefaultContainerDragBehavior : IContainerDragBehavior
    {
        private readonly FrameworkElement associatedObject;

        public event MouseEventHandler MouseLeave;

        public event MouseButtonEventHandler MouseButtonUp;

        public event MouseButtonEventHandler MouseButtonDown;

        public DefaultContainerDragBehavior(IFrameworkInputElement element)
        {
            associatedObject = element as FrameworkElement;
        }

        public void OnAttached()
        {
            if (associatedObject == null)
            {
                return;
            }

            associatedObject.MouseLeave += ElementOnMouseLeave;
            associatedObject.MouseLeftButtonUp += ElementOnMouseLeftButtonUp;
            associatedObject.MouseLeftButtonDown += ElementOnMouseLeftButtonDown;
        }

        private void ElementOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MouseButtonDown?.Invoke(sender, e);
        }

        private void ElementOnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            MouseButtonUp?.Invoke(sender, e);
        }

        private void ElementOnMouseLeave(object sender, MouseEventArgs e)
        {
            MouseLeave?.Invoke(sender, e);
        }
    }
}
