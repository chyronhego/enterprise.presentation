﻿namespace ChyronHego.Enterprise.Presentation.Behaviors.DragDrop
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;

    public class GridContainerDragBehavior : IContainerDragBehavior
    {
        private readonly FrameworkElement associatedObject;

        public event MouseEventHandler MouseLeave;

        public event MouseButtonEventHandler MouseButtonUp;

        public event MouseButtonEventHandler MouseButtonDown;

        public GridContainerDragBehavior(IFrameworkInputElement element)
        {
            associatedObject = element as FrameworkElement;
        }

        public void OnAttached()
        {
            if (associatedObject is DataGrid dataGrid)
            {
                dataGrid.LoadingRow += (sender, args) =>
                {
                    args.Row.MouseLeave += RowOnMouseLeave;
                    args.Row.PreviewMouseUp += RowOnPreviewMouseUp;
                    args.Row.PreviewMouseDown += RowOnPreviewMouseDown;
                };
            }
        }

        private void RowOnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            MouseButtonDown?.Invoke(sender, e);
        }

        private void RowOnPreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            MouseButtonUp?.Invoke(sender, e);
        }

        private void RowOnMouseLeave(object sender, MouseEventArgs e)
        {
            MouseLeave?.Invoke(sender, e);
        }
    }
}
