﻿namespace ChyronHego.Enterprise.Presentation.Behaviors.DragDrop
{
    using System.Windows.Input;

    public interface IContainerDragBehavior : IContainerBehavior
    {
        event MouseEventHandler MouseLeave;

        event MouseButtonEventHandler MouseButtonUp;

        event MouseButtonEventHandler MouseButtonDown;
    }
}
