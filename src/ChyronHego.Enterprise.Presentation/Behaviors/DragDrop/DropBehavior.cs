﻿namespace ChyronHego.Enterprise.Presentation.Behaviors.DragDrop
{
    using System.Windows;
    using System.Windows.Interactivity;

    public class DropBehavior : Behavior<FrameworkElement>
    {
        private readonly IContainerDropBehavior containerBehavior;

        public DropBehavior()
        {
            containerBehavior = 
                ContainerBehaviorFactory.CreateBehaviorStrategy<IContainerDropBehavior>(AssociatedObject, BehaviorTypes.Drop);
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.AllowDrop = true;
            containerBehavior.OnAttached();
        }
    }
}
