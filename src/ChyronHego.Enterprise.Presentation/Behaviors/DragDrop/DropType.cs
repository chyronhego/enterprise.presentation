﻿namespace ChyronHego.Enterprise.Presentation.Behaviors.DragDrop
{
    public enum DropType
    {
        Insert,
        Reorder
    }
}
