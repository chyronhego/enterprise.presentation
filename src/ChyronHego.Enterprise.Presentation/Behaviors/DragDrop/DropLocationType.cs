﻿namespace ChyronHego.Enterprise.Presentation.Behaviors.DragDrop
{
    public enum DropLocationType
    {
        Above,
        Below,
        Inside
    }
}
