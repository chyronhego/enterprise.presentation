﻿namespace ChyronHego.Enterprise.Presentation.Behaviors.DragDrop
{
    using System.Windows;

    public interface IContainerDropBehavior : IContainerBehavior
    {
        void OnDragEnter(object sender, DragEventArgs e);

        void OnDragOver(object sender, DragEventArgs e);

        void OnDragLeave(object sender, DragEventArgs e);

        void OnDragDrop(object sender, DragEventArgs e);
    }
}
