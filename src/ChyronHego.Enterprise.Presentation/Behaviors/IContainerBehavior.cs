﻿namespace ChyronHego.Enterprise.Presentation.Behaviors
{
    using System.Windows;

    public interface IContainerBehavior
    {
        void OnAttached();
    }
}
