﻿namespace ChyronHego.Enterprise.Presentation.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    public class MultiplyWithConstantConverter : BaseConverter, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((value is double doubleValue) && (parameter is double constant))
            {
                return doubleValue * constant;
            }

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotImplementedException();
    }
}