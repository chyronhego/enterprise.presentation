﻿namespace ChyronHego.Enterprise.Presentation.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    public class FloatToDecimalConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is float floatValue)
            {
                return floatValue;
            }
            else
            {
                return Convert.ToSingle(value);
            }
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {

            if (value is decimal decimalValue)
            {
                return decimalValue;
            }
            else
            {
                return Convert.ToDecimal(value);
            }
        }
    }
}
