﻿namespace ChyronHego.Enterprise.Presentation.Converters
{
    using System;
    using System.Drawing;
    using System.Globalization;
    using System.Windows.Data;
    using System.Windows.Media.Imaging;
    using WpfControls = System.Windows.Controls;

    public class ImageToImageControlConverter : BaseConverter, IValueConverter
    {
        private ImageToImageSourceConverter imageSourceConverter = new ImageToImageSourceConverter();
        
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Image imageValue)
            {
                var source = imageSourceConverter.Convert(value, targetType, parameter, culture);
                if (source is BitmapImage bitmapImageValue)
                {
                    return new WpfControls.Image()
                    {
                        Source = bitmapImageValue
                    };
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
