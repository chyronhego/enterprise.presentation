﻿using ChyronHego.Enterprise.Presentation.Extensions;

namespace ChyronHego.Enterprise.Presentation.Converters
{
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Globalization;
    using System.IO;
    using System.Windows.Data;
    using System.Windows.Media.Imaging;

    public class ImageToImageSourceConverter : BaseConverter, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Image imageValue)
            {
                return imageValue.ToImageSource(ImageFormat.Bmp);
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
