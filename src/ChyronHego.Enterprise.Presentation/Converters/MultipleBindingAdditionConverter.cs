﻿namespace ChyronHego.Enterprise.Presentation.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    public class MultipleBindingAdditionConverter : BaseConverter, IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            double returnValue = 0;
            foreach (var value in values)
            {
                if (value is double valToAdd)
                {
                    returnValue += valToAdd;
                }
            }

            if (parameter is double paramValue)
            {
                returnValue += paramValue;
            }

            return returnValue;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}