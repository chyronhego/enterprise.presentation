﻿namespace ChyronHego.Enterprise.Presentation.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Windows.Data;
    using ChyronHego.Enterprise.Presentation.Objects;

    public class QualityControlConverter : BaseConverter, IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if ((parameter is string parameterName)
                && (values[0] is string qualityValue)
                && (values[1] is IList<QualityControlInfo> qualityControlValues))
            {
                var qualityInfo = qualityControlValues.FirstOrDefault(val => val.Name == qualityValue);
                switch (parameterName)
                {
                    case nameof(QualityControlInfo.Name):
                        return qualityInfo?.Name;
                    case nameof(QualityControlInfo.Foreground):
                        return qualityInfo?.Foreground;
                    case nameof(QualityControlInfo.Background):
                        return qualityInfo?.Background;
                    case "Value":
                        return qualityControlValues.IndexOf(qualityInfo);
                }
            }

            return null;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}