﻿namespace ChyronHego.Enterprise.Presentation.Converters
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Media;

    public class TileViewTextItemHeightConverter : BaseConverter, IMultiValueConverter
    {
        private const int ReservedIndexes = 7;
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {           
            double height = 0;            
            if (values.Length > ReservedIndexes
                && values[0] is string text
                && values[1] is double width
                && values[2] is FontFamily family
                && values[3] is FontStyle style
                && values[4] is FontWeight weight
                && values[5] is FontStretch stretch
                && values[6] is double size)
            {
                var textBlock = new TextBlock
                {
                    Text = text,
                    TextWrapping = TextWrapping.Wrap,
                    FontFamily = family,
                    FontStyle = style,
                    FontStretch = stretch,
                    FontWeight = weight,
                    FontSize = size,
                };

                textBlock.Measure(new Size(Double.PositiveInfinity, Double.PositiveInfinity));
                textBlock.Arrange(new Rect(textBlock.DesiredSize));

                double lineHeight = textBlock.ActualHeight;

                textBlock.Measure(new Size(width, Double.PositiveInfinity));
                textBlock.Arrange(new Rect(textBlock.DesiredSize));

                double actualHeight = textBlock.ActualHeight;

                int lines = (int)(actualHeight / lineHeight);               
                
                if (lines <= 0)
                {
                    lines = 1;
                }
                else if (parameter is int maxNumberOfLines)
                {
                    lines = Math.Min(lines, maxNumberOfLines);
                }

                height = lineHeight * lines;
            }

            for (int index = ReservedIndexes; index < values.Length; index++)
            {
                if (values[index] is double value)
                {
                    height += value;
                }
            }

            return height;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}