﻿namespace ChyronHego.Enterprise.Presentation.Converters
{
    using System;
    using System.Windows.Markup;


    /// <summary>
    /// Base class to use for Converters that allows use of converters in
    /// XAML without requiring the instantiation of a static resource
    /// </summary>
    public abstract class BaseConverter : MarkupExtension
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
