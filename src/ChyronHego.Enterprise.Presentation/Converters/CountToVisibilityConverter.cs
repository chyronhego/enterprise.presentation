﻿namespace ChyronHego.Enterprise.Presentation.Converters
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Data;

    /// <summary>
    /// Class used to convert a count into a visibility.  Main use case is when the programmer wishes to hide a menu item
    /// or other GUI element when there is no content in a list of items.  Pivot point can be adjusted as well as whether
    /// the pivot triggers visible, collapsed, or hidden
    /// </summary>
    public class CountToVisibilityConverter : BaseConverter, IValueConverter
    {
        public Visibility GreaterThanPivotValue { get; set; } = Visibility.Visible;

        public Visibility LessThanPivotValue { get; set; } = Visibility.Hidden;

        public Visibility EqualToPivotValue { get; set; } = Visibility.Hidden;

        public int Pivot { get; set; } = 0;

        private static int Count(IEnumerable enumerable)
        {
            int count = 0;

            if (enumerable.GetEnumerator() is IEnumerator enumerator)
            {
                enumerator.Reset();
                while (enumerator.MoveNext())
                {
                    count++;
                }
            }

            return count;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int intValue;

            if (value == null)
            {
                intValue = 0;
            }
            else
            {
                switch (value)
                {
                    case int i:
                        intValue = i;
                        break;
                    case string s:
                        intValue = s.Length;
                        break;
                    case ICollection c:
                        intValue = c.Count;
                        break;
                    case IEnumerable<object> enumerableObject:
                        intValue = enumerableObject.Count();
                        break;
                    case IEnumerable enumerable:
                        intValue = Count(enumerable);
                        break;
                    default:
                        return null;
                }
            }

            if (intValue < Pivot)
            {
                return LessThanPivotValue;
            }
            else if (intValue > Pivot)
            {
                return GreaterThanPivotValue;
            }
            else if (intValue == Pivot)
            {
                return EqualToPivotValue;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
