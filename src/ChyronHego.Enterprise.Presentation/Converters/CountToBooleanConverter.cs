﻿namespace ChyronHego.Enterprise.Presentation.Converters
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Windows.Data;

    public class CountToBooleanConverter : BaseConverter, IValueConverter
    {
        public bool GreaterThanPivotValue { get; set; } = true;

        public bool LessThanPivotValue { get; set; } = false;

        public bool EqualToPivotValue { get; set; } = false;

        public int Pivot { get; set; } = 0;

        private static int Count(IEnumerable enumerable)
        {
            int count = 0;

            if (enumerable.GetEnumerator() is IEnumerator enumerator)
            {
                enumerator.Reset();
                while (enumerator.MoveNext())
                {
                    count++;
                }
            }

            return count;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int intValue;

            if (value == null)
            {
                intValue = 0;
            }
            else
            {
                switch (value)
                {
                    case int i:
                        intValue = i;
                        break;
                    case string s:
                        intValue = s.Length;
                        break;
                    case ICollection c:
                        intValue = c.Count;
                        break;
                    case IEnumerable<object> enumerableObject:
                        intValue = enumerableObject.Count();
                        break;
                    case IEnumerable enumerable:
                        intValue = Count(enumerable);
                        break;
                    default:
                        return null;
                }
            }

            if (intValue < Pivot)
            {
                return LessThanPivotValue;
            }
            else if (intValue > Pivot)
            {
                return GreaterThanPivotValue;
            }
            else if (intValue == Pivot)
            {
                return EqualToPivotValue;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
