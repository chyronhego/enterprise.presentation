﻿namespace ChyronHego.Enterprise.Presentation.Converters
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;

    public class MultiBindingBoolToVisibilityConverter : MultiBoolConverter, IMultiValueConverter
    {
        public Visibility TrueValue { get; set; }
        public Visibility FalseValue { get; set; }

        public MultiBindingBoolToVisibilityConverter()
        {
            TrueValue = Visibility.Visible;
            FalseValue = Visibility.Collapsed;
            CombinationOperator = CombineOperatorEnum.AndValuesTogether;
        }

        public override object Convert(object[] values, Type targetType, object parameter,
            CultureInfo culture)
        {
            bool? result = GetValue(values);

            if (result == null)
            {
                return null;
            }

            return result.Value ? TrueValue : FalseValue;
        }

        public override object[] ConvertBack(object value, Type[] targetTypes, object parameter,
            CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
