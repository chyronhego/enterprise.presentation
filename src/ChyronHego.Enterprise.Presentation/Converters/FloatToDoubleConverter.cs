﻿namespace ChyronHego.Enterprise.Presentation.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    public class FloatToDoubleConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is float floatValue)
            {
                return (double) floatValue;
            }
            else
            {
                return Convert.ToDouble(value);
            }
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double doubleValue)
            {
                return (float) doubleValue;
            }
            else
            {
                return Convert.ToSingle(value);
            }
        }
    }
}
