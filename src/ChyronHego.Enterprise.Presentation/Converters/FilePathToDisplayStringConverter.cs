﻿namespace ChyronHego.Framework.Presentation.Converters
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Windows.Data;

    public class FilePathToDisplayStringConverter : BaseConverter, IValueConverter
    {
        public bool IncludeExtension { get; set; } = false;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string stringValue)
            {
                return IncludeExtension
                    ? Path.GetFileName(stringValue)
                    : Path.GetFileNameWithoutExtension(stringValue);
            }

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
