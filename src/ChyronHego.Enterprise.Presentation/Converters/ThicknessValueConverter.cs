﻿namespace ChyronHego.Enterprise.Presentation.Converters
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;

    public class ThicknessValueConverter : BaseConverter, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool horizontal = true;
            if (parameter is bool)
            {
                horizontal = (bool)parameter;
            }

            if((value is Thickness thickness))
            {
                return horizontal ? thickness.Left + thickness.Right : thickness.Top + thickness.Bottom;
            }

            return 0;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotImplementedException();
    }
}
