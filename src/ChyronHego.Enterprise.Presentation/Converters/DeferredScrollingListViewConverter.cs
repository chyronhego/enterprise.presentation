﻿namespace ChyronHego.Enterprise.Presentation.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;
    using ChyronHego.Enterprise.Presentation.Controls;
    using ChyronHego.Enterprise.Presentation.Controls.CustomControls;

    public class DeferredScrollingListViewConverter : BaseConverter, IMultiValueConverter
    {
        private const int DefaultListViewSmoothScrollingLimit = 100;
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if ( (values.Length == 5) && (values[0] is TileView view)
                && (values[1] is double viewWidth) && (values[2] is double viewHeight)
                && (values[3] is double itemWidth) && (values[4] is double itemHeight))
            {
                if (!Double.IsNaN(viewWidth) && !Double.IsNaN(viewWidth) && !Double.IsNaN(itemWidth) && !Double.IsNaN(itemHeight))
                {
                    int rows = (int)(viewHeight / itemHeight);
                    int columns = (int)(viewWidth / itemWidth);
                    return rows * columns > DefaultListViewSmoothScrollingLimit;
                }                
            }            

            return false;
        }       


        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }    
    }
}
