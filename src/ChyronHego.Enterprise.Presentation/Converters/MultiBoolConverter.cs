﻿namespace ChyronHego.Enterprise.Presentation.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    public enum CombineOperatorEnum
    {
        AndValuesTogether,
        OrValuesTogether
    }

    public class MultiBoolConverter : BaseConverter, IMultiValueConverter
    {
        public CombineOperatorEnum CombinationOperator { get; set; }

        public virtual object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return GetValue(values);
        }

        public virtual object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        protected bool? GetValue(object[] values)
        {
            // if we are supposed to AND all the values together then we start 
            // with True, otherwise, if we are supposed to OR all the values together
            // then we start with false
            var result = (CombinationOperator == CombineOperatorEnum.AndValuesTogether);

            if (values == null || values.Length <= 0)
            {
                return null;
            }

            foreach (var value in values)
            {
                if (value is bool boolValue)
                {
                    switch (CombinationOperator)
                    {
                        case CombineOperatorEnum.AndValuesTogether:
                            result &= boolValue;
                            break;
                        case CombineOperatorEnum.OrValuesTogether:
                            result |= boolValue;
                            break;
                        default:
                            break;
                    }
                }
            }

            return result;
        }
    }
}
