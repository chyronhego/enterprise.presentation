﻿namespace ChyronHego.Enterprise.Presentation.Extensions
{
    public static class LongExtensions
    {
        public static string ToHumanReadableFileSize(this long input, int precision = 0)
        {
            long absolute_i = (input < 0 ? -input : input);

            string suffix;
            double readable;
            if (absolute_i >= 0x1000000000000000) // Exabyte
            {
                suffix = "EB";
                readable = (input >> 50);
            }
            else if (absolute_i >= 0x4000000000000) // Petabyte
            {
                suffix = "PB";
                readable = (input >> 40);
            }
            else if (absolute_i >= 0x10000000000) // Terabyte
            {
                suffix = "TB";
                readable = (input >> 30);
            }
            else if (absolute_i >= 0x40000000) // Gigabyte
            {
                suffix = "GB";
                readable = (input >> 20);
            }
            else if (absolute_i >= 0x100000) // Megabyte
            {
                suffix = "MB";
                readable = (input >> 10);
            }
            else if (absolute_i >= 0x400) // Kilobyte
            {
                suffix = "KB";
                readable = input;
            }
            else
            {
                return input.ToString("0 B"); // Byte
            }
            readable = (readable / 1024);

            var precisionString = "";
            for (int i = 0; i < precision; i++)
            {
                precisionString += "#";
            }

            return readable.ToString($"0.{precisionString} ") + suffix;
        }
    }
}
