﻿namespace ChyronHego.Enterprise.Presentation.Extensions
{
    using System.Windows;
    using DrawingSize = System.Drawing.Size;
    using DrawingSizeF = System.Drawing.SizeF;

    public static class SizeExtensions
    {
        public static DrawingSize ToDrawingSize(this Point point)
        {
            return new DrawingSize((int)point.X, (int)point.Y);
        }

        public static DrawingSizeF ToDrawingSizeF(this Point point)
        {
            return new DrawingSizeF((float)point.X, (float)point.Y);
        }
    }
}
