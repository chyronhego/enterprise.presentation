﻿namespace ChyronHego.Enterprise.Presentation.Extensions
{
    using System.Windows.Data;

    public static class BindingOperationsEx
    {
        public static Binding CreateTwoWayBinding(object source, string property, IValueConverter converter = null)
        {
            return new Binding(property)
            {
                Source = source,
                Mode = BindingMode.TwoWay,
                Converter = converter
            };
        }

        public static Binding CreateOneWayBinding(object source, string property, IValueConverter converter = null)
        {
            return new Binding(property)
            {
                Source = source,
                Mode = BindingMode.OneWay,
                Converter = converter
            };
        }
    }
}