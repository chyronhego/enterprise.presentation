﻿namespace ChyronHego.Enterprise.Presentation.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Interop;
    using System.Windows.Media;
    using System.Windows.Threading;
    using ChyronHego.Enterprise.Presentation.Controls.Properties;

    public static class ControlExtensions
    {
        public static string GetBindingValue(this GridViewColumn col, object src)
        {
            var bindingName = (col.DisplayMemberBinding as Binding)?.Path?.Path;
            return bindingName != null ? GetPropValue(src, bindingName) as string : null;
        }

        public static string GetName(this GridViewColumn col)
        {
            string name = col.GetValue(GridViewColumnProperties.NameProperty).ToString();
            if (name == null)
            {
                name = col.Header.ToString();
            }

            return name;
        }

        public static object GetPropValue(object src, string propName)
        {
            var indexArray = GetPropertyArrayIndex(propName);
            var objectType = src.GetType();
            if (indexArray != null)
            {
                var subProperty = GetArrayPropertyName(propName);
                if (subProperty != null)
                {
                    var property = objectType.GetProperty(subProperty);
                    objectType = property?.PropertyType;
                    src = property?.GetValue(src);
                }

                if (objectType != null)
                {
                    return objectType.GetProperty("Item").GetValue(src, indexArray);
                }
            }

            return objectType?.GetProperty(propName)?.GetValue(src, indexArray);
        }

        private static string GetArrayPropertyName(string prop)
        {
            var matches = Regex.Match(prop, @".+\[");
            if (matches.Success)
            {
                var array = new string[matches.Captures.Count];
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = matches.Captures[i].Value.Trim(new char[] { '[' });
                }

                return array[0];
            }

            return null;
        }

        private static object[] GetPropertyArrayIndex(string prop)
        {
            var matches = Regex.Match(prop, @"\[[(0-9)]+\]+");
            if (matches.Success)
            {
                var array = new object[matches.Captures.Count];
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = int.Parse(matches.Captures[i].Value.Trim(new char[] { '[', ']' }));
                }

                return array;
            }

            return null;
        }

        public static DispatcherOperation SafeInvoke(this Dispatcher dispatcher, Action action)
        {
            return dispatcher.SafeInvoke(action, DispatcherPriority.Normal);
        }

        public static DispatcherOperation SafeInvoke(this Dispatcher dispatcher, Action action, DispatcherPriority priority)
        {
            if (dispatcher != null && !dispatcher.CheckAccess())
            {
                return dispatcher.BeginInvoke(action, priority);
            }
            else
            {
                action();
                return null;
            }
        }

        public static T GetTopLevelControl<T>(object source) where T : DependencyObject
        {
            var selectedItem = source as DependencyObject;

            while (selectedItem != null)
            {
                if (selectedItem is T value)
                {
                    return value;
                }

                selectedItem = VisualTreeHelper.GetParent(selectedItem);
            }

            return default(T);
        }

        public static T FindChild<T>(DependencyObject reference) where T : class
        {
            // Do a breadth first search.
            var queue = new Queue<DependencyObject>();
            queue.Enqueue(reference);
            while (queue.Count > 0)
            {
                var child = queue.Dequeue();
                T obj = child as T;
                if (obj != null)
                {
                    return obj;
                }

                // Add the children to the queue to search through later.
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(child); i++)
                {
                    queue.Enqueue(VisualTreeHelper.GetChild(child, i));
                }
            }
            return null; // Not found.
        }

        public static System.Drawing.Point GetPixelSize(this FrameworkElement visual)
        {
            Matrix matrix;
            var source = PresentationSource.FromVisual(visual);
            if (source != null)
            {
                matrix = source.CompositionTarget.TransformToDevice;
            }
            else
            {
                using (var hwndSource = new HwndSource(new HwndSourceParameters()))
                {
                    matrix = hwndSource.CompositionTarget.TransformToDevice;
                }
            }
            var renderSize = visual.RenderSize;

            int pixelX = (int)Math.Max(int.MinValue, Math.Min(int.MaxValue, matrix.M11 * renderSize.Width));
            int pixelY = (int)Math.Max(int.MinValue, Math.Min(int.MaxValue, matrix.M22 * renderSize.Height));

            return new System.Drawing.Point(pixelX, pixelY);
        }
    }
}