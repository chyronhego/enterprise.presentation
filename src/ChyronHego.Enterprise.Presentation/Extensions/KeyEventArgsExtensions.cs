﻿namespace ChyronHego.Enterprise.Presentation.Extensions
{
    using System.Collections.Generic;
    using FormsKeyEventArgs = System.Windows.Forms.KeyEventArgs;
    using Key = System.Windows.Input.Key;
    using KeyInterop = System.Windows.Input.KeyInterop;
    using Keys = System.Windows.Forms.Keys;
    using ModifierKeys = System.Windows.Input.ModifierKeys;
    using WpfKeyEventArgs = System.Windows.Input.KeyEventArgs;

    public static class KeyEventArgsExtensions
    {
        private static readonly Dictionary<ModifierKeys, Keys> modifierMapping = new Dictionary<ModifierKeys, Keys>()
        {
            { ModifierKeys.Alt, Keys.Alt },
            { ModifierKeys.Control, Keys.Control},
            { ModifierKeys.Shift, Keys.Shift},
            { ModifierKeys.Windows, Keys.LWin},
        };

        public static FormsKeyEventArgs ToWinforms(this WpfKeyEventArgs keyEventArgs)
        {
            var wpfKey = keyEventArgs.Key == Key.System ? keyEventArgs.SystemKey : keyEventArgs.Key;
            var winformModifiers = ToWinforms(keyEventArgs.KeyboardDevice.Modifiers);
            var winformKeys = (Keys)KeyInterop.VirtualKeyFromKey(wpfKey);

            var strippedKeys = StripLeftRight(winformKeys);

            return new FormsKeyEventArgs(strippedKeys | winformModifiers);
        }

        public static (Key key, ModifierKeys modifiers) ToWpf(this FormsKeyEventArgs keyEventArgs)
        {
            var modifiersValue = GetModifiers(keyEventArgs.KeyData);

            var key = KeyInterop.KeyFromVirtualKey((int)keyEventArgs.KeyCode);

            return (key, modifiersValue);
        }

        private static ModifierKeys GetModifiers(Keys keys)
        {
            var modifiersValue = ModifierKeys.None;

            foreach (var pair in modifierMapping)
            {
                if (keys.HasFlag(pair.Value))
                {
                    modifiersValue |= pair.Key;
                }
            }

            return modifiersValue;
        }

        private static Keys StripLeftRight(Keys keys)
        {
            switch (keys)
            {
                case Keys.LShiftKey:
                case Keys.RShiftKey:
                    return Keys.ShiftKey;
                case Keys.LControlKey:
                case Keys.RControlKey:
                    return Keys.ControlKey;
                case Keys.LMenu:
                case Keys.RMenu:
                    return Keys.Menu;
                default:
                    return keys;
            }
        }

        private static Keys ToWinforms(ModifierKeys modifier)
        {
            var keyValue = Keys.None;

            foreach (var pair in modifierMapping)
            {
                if (modifier.HasFlag(pair.Key))
                {
                    keyValue |= pair.Value;
                }
            }

            return keyValue;
        }
    }
}
