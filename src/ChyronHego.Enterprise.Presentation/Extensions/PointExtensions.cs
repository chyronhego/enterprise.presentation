﻿namespace ChyronHego.Enterprise.Presentation.Extensions
{
    using System.Windows;
    using DrawingPoint = System.Drawing.Point;
    using DrawingPointF = System.Drawing.PointF;

    public static class PointExtensions
    {
        public static DrawingPoint ToDrawingPoint(this Point point)
        {
            return new DrawingPoint((int)point.X, (int)point.Y);
        }

        public static DrawingPointF ToDrawingPointF(this Point point)
        {
            return new DrawingPointF((float)point.X, (float)point.Y);
        }
    }
}
