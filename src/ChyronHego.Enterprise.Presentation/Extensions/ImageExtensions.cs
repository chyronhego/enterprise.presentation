﻿namespace ChyronHego.Enterprise.Presentation.Extensions
{
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    public static class ImageExtensions
    {
        public static ImageSource ToImageSource(this Image image, ImageFormat imageFormat)
        {
            BitmapImage bitmap = new BitmapImage();

            using (MemoryStream stream = new MemoryStream())
            {
                image.Save(stream, imageFormat);
                stream.Seek(0, SeekOrigin.Begin);

                bitmap.BeginInit();
                bitmap.StreamSource = stream;
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.EndInit();
            }

            // freeze to allow cross-thread access and better performance
            bitmap.Freeze();

            return bitmap;
        }
    }
}

