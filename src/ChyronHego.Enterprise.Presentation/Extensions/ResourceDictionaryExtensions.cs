﻿namespace ChyronHego.Enterprise.Presentation.Extensions
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows;

    public static class ResourceDictionaryExtensions
    {
        public static void Add(this Collection<ResourceDictionary> mergedDictionaries, string uri, UriKind uriKind = UriKind.Relative)
        {
            if (mergedDictionaries == null || string.IsNullOrWhiteSpace(uri))
            {
                return;
            }

            Add(mergedDictionaries, new Uri(uri, uriKind));
        }

        public static void Add(this Collection<ResourceDictionary> mergedDictionaries, Uri uri)
        {
            if (mergedDictionaries == null || uri == null)
            {
                return;
            }

            mergedDictionaries.Add((ResourceDictionary)Application.LoadComponent(uri));
        }
    }
}
