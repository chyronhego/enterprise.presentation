﻿namespace ChyronHego.Enterprise.Presentation.Extensions
{
    using System;
    using System.Windows;
    using System.Windows.Interop;
    using System.Windows.Media;
    using ChyronHego.Enterprise.Logging;

    internal class WindowOwner : System.Windows.Forms.IWin32Window
    {
        public WindowOwner(Window window)
        {
            Handle = new WindowInteropHelper(window).Handle;
        }

        public WindowOwner(HwndSource source)
        {
            Handle = source.Handle;
        }

        public IntPtr Handle { get; }
    }

    public static class DialogExtensions
    {
        private static System.Windows.Forms.IWin32Window GetOwnerForVisual(Visual visual)
        {
            var window = Window.GetWindow(visual);

            if (window != null)
            {
                return new WindowOwner(window);
            }

            // below is necessary for when this control is not hosted in a WPF window
            if (PresentationSource.FromVisual(visual) is HwndSource source)
            {
                return new WindowOwner(source);
            }

            return null;
        }

        public static System.Windows.Forms.DialogResult ShowDialog(this System.Windows.Forms.Form dialog, Visual visual)
        {
            var owner = GetOwnerForVisual(visual);
            if (owner != null)
            {
                return dialog.ShowDialog(owner);
            }

            LogManager.Log(nameof(DialogExtensions), "Failed to find a window to set as owner for dialog");

            return dialog.ShowDialog();
        }

        /// <summary>
        /// Shows dialog and tries to find owner by looking for WPF Window or Winforms host
        /// </summary>
        /// <param name="dialog"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public static System.Windows.Forms.DialogResult ShowDialog(this System.Windows.Forms.CommonDialog dialog, Visual visual)
        {
            var owner = GetOwnerForVisual(visual);
            if (owner != null)
            {
                return dialog.ShowDialog(owner);
            }

            LogManager.Log(nameof(DialogExtensions), "Failed to find a window to set as owner for dialog");

            return dialog.ShowDialog();
        }
    }
}
