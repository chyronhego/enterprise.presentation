﻿namespace ChyronHego.Enterprise.Presentation.Extensions
{
    using System;
    using System.Windows;
    using System.Windows.Input;

    public static class UIElementExtensions
    {
        public static void AddCommandBinding(this UIElement uiElement, ICommand command)
        {
            uiElement.AddCommandBinding(command,
                (sender, args) => command?.Execute(sender),
                (sender, args) => args.CanExecute = command?.CanExecute(sender) ?? true);
        }

        public static void AddCommandBinding(this UIElement uiElement, ICommand command,
            Action execute, Func<bool> canExecute = null)
        {
            if (uiElement == null || command == null)
            {
                return;
            }

            uiElement.CommandBindings.Add(new CommandBinding(
                command,
                (sender, args) => execute?.Invoke(),
                (sender, args) => args.CanExecute = canExecute?.Invoke() ?? true));
        }

        public static void AddCommandBinding(this UIElement uiElement,
            ICommand command,
            ExecutedRoutedEventHandler execute,
            CanExecuteRoutedEventHandler canExecute = null)
        {
            if (uiElement == null || command == null)
            {
                return;
            }

            uiElement.CommandBindings.Add(new CommandBinding(command, execute, canExecute));
        }
    }
}
