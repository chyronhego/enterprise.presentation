﻿namespace ChyronHego.Enterprise.Presentation.Extensions
{
    using System.Windows.Media;

    public static class ColorExtensions
    {
        public static Color GetColorDarker(this Color color, double factor)
        {
            if (factor < 0 || factor > 1)
                return color;

            byte r = (byte)(factor * color.R);
            byte g = (byte)(factor * color.G);
            byte b = (byte)(factor * color.B);
            return Color.FromArgb(color.A, r, g, b);
        }

        public static Color GetColorLighter(this Color color, double factor)
        {
            if (factor < 0 || factor > 1)
                return color;

            byte r = (byte)(factor * color.R + (1 - factor) * 255);
            byte g = (byte)(factor * color.G + (1 - factor) * 255);
            byte b = (byte)(factor * color.B + (1 - factor) * 255);
            return Color.FromArgb(color.A, r, g, b);
        }
    }
}
