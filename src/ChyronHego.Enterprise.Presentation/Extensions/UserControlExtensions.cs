﻿namespace ChyronHego.Enterprise.Presentation.Extensions
{
    using System;
    using ChyronHego.Enterprise.Presentation.Controls;
    using ChyronHego.Enterprise.Presentation.Controls.UserControls;
    using ChyronHego.Enterprise.Presentation.Interfaces;
    using WinFormsUserControl = System.Windows.Forms.UserControl;
    using WpfUserControl = System.Windows.Controls.UserControl;

    public static class UserControlExtensions
    {
        public static WpfUserControl AsWpfUserControl(this IUserControl userControl)
        {
            if (userControl is WpfUserControl wpfUserControl)
            {
                return wpfUserControl;
            }

            if (userControl is WinFormsUserControl winFormsUserControl)
            {
                return new WinFormsUserControlWrapper(winFormsUserControl);
            }

            return null;
        }
    }
}
