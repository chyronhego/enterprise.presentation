﻿namespace ChyronHego.Enterprise.Presentation.Extensions
{
    using System;
    using System.Windows.Markup;

    public class EnumBindingSourceExtension : MarkupExtension
    {
        private Type type;

        public EnumBindingSourceExtension(Type enumType)
        {
            EnumType = enumType;
        }

        public Type EnumType
        {
            get { return type; }
            set
            {
                if (value != type)
                {
                    if (value != null)
                    {
                        var enumType = Nullable.GetUnderlyingType(value) ?? value;

                        if (!enumType.IsEnum)
                        {
                            throw new ArgumentException("Type must be for an Enum.");
                        }
                    }

                    type = value;
                }
            }
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (type == null)
            {
                throw new InvalidOperationException("The EnumType must be specified.");
            }

            var enumType = Nullable.GetUnderlyingType(type) ?? type;

            var values = Enum.GetValues(enumType);

            if (enumType == type)
            {
                return values;
            }

            var array = Array.CreateInstance(enumType, values.Length + 1);

            values.CopyTo(array, 1);

            return array;
        }
    }
}
