﻿namespace ChyronHego.Enterprise.Presentation.Controls
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using ChyronHego.Enterprise.Presentation.Controls.Enums;
    using ChyronHego.Enterprise.Presentation.Controls.Interfaces;

    public class ReorderableDataGridHelper<Item> where Item : class, IReorderableRow
    {
        private readonly DataGrid grid;
        private int? dragIndex = null;

        private IList<Item> Items => grid.ItemsSource as IList<Item>;

        public delegate void OnItemMovedEvent(Item item, int targetIndex);
        public event OnItemMovedEvent ItemMoved;

        public ReorderableDataGridHelper(DataGrid grid)
        {
            this.grid = grid;
        }

        private DataGridRow GetRowFromControl(DependencyObject control)
        {
            while (control != null && !(control is DataGridRow) && control != grid)
            {
                control = VisualTreeHelper.GetParent(control);
            }
            if (control is DataGridRow row)
            {
                return row;
            }
            return null;
        }

        private int? FindRowIndexFromControl(DependencyObject control)
        {
            var row = GetRowFromControl(control);
            if (row != null)
            {
                return grid.ItemContainerGenerator.IndexFromContainer(row);
            }
            return null;
        }

        public void MouseMove(object sender, MouseEventArgs e)
        {
            if (!dragIndex.HasValue)
            {
                return;
            }

            var hit = grid.InputHitTest(e.GetPosition(grid));
            var row = GetRowFromControl(hit as DependencyObject);
            Item dropTarget = null;

            if (row != null)
            {
                dropTarget = grid.ItemContainerGenerator.ItemFromContainer(row) as Item;

                if (dropTarget != null)
                {
                    dropTarget.ReorderDropState = e.GetPosition(row).Y > row.ActualHeight / 2
                        ? ReorderDropState.Below
                        : ReorderDropState.Above;
                }
            }

            foreach (var source in Items)
            {
                if (source != dropTarget)
                {
                    source.ReorderDropState = ReorderDropState.None;
                }
            }
        }

        public void Drop()
        {
            if (dragIndex == null)
            {
                return;
            }

            var dropTarget = Items.FirstOrDefault(s => s.ReorderDropState != ReorderDropState.None);

            if (dropTarget != null)
            {
                int targetIndex = Items.IndexOf(dropTarget);

                if (dropTarget.ReorderDropState == ReorderDropState.Below)
                {
                    targetIndex++;
                }

                dropTarget.ReorderDropState = ReorderDropState.None;
                var item = Items[dragIndex.Value];

                ItemMoved?.Invoke(item, targetIndex);

                grid.SelectedItem = item;
                grid.Focus();
            }

            dragIndex = null;
        }

        public void StartDrag(DependencyObject source)
        {
            // datagrid enters corrupt state when moving item during edit
            bool couldCommit = grid.CommitEdit();
            if (!couldCommit)
            {
                grid.CancelEdit();
            }

            dragIndex = FindRowIndexFromControl(source);
        }
    }
}
