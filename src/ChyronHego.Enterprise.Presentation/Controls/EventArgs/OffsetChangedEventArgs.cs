﻿namespace ChyronHego.Enterprise.Presentation.Controls.EventArgs
{
    using System;

    public sealed class OffsetChangedEventArgs : EventArgs
    {
        private readonly double horizontal;
        private readonly double vertical;

        internal OffsetChangedEventArgs(double horizontal, double vertical)
        {
            this.horizontal = horizontal;
            this.vertical = vertical;
        }

        public double HorizontalChange
        {
            get { return horizontal; }
        }

        public double VerticalChange
        {
            get { return vertical; }
        }
    }
}