﻿namespace ChyronHego.Enterprise.Presentation.Controls.EventArgs
{
    using System;
    using System.ComponentModel;
    using System.Windows.Controls;

    public class ColumnSortedEventArgs : EventArgs
    {
        public GridViewColumn Column { get; }

        public ListSortDirection Direction { get; }

        public ColumnSortedEventArgs(GridViewColumn column, ListSortDirection direction)
        {
            Column = column;
            Direction = direction;
        }
    }
}