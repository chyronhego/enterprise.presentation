﻿namespace ChyronHego.Enterprise.Presentation.Controls.CustomControls
{
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Input;

    /// <summary>
    /// This is a custom button used to imitate the win forms toolstrip DropDownButton
    /// If you set the ContextMenu on this type of button, the context menu will be displayed
    /// when the user selects the button (and normal button-press behavior will not fire)
    /// this button can also imitate the win forms toolstrip split button by setting the
    /// UseSplitDropDown property to true
    /// </summary>
    public class ContextMenuButton : Button
    {
        private static int splitButtonWidth = 12;

        static ContextMenuButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ContextMenuButton),
                new FrameworkPropertyMetadata(typeof(ContextMenuButton)));
        }

        public static readonly DependencyProperty UseSplitDropDownProperty = DependencyProperty.Register(nameof(UseSplitDropDown),
            typeof(bool), typeof(ContextMenuButton), new PropertyMetadata(false));

        public bool UseSplitDropDown
        {
            get => (bool)GetValue(UseSplitDropDownProperty);
            set => SetValue(UseSplitDropDownProperty, value);
        }

        internal static readonly DependencyPropertyKey IsSplitPressedPropertyKey =
            DependencyProperty.RegisterReadOnly(
                "IsSplitPressed",
                typeof(bool),
                typeof(ContextMenuButton),
                new PropertyMetadata(false));

        public static readonly DependencyProperty IsSplitPressedProperty = IsSplitPressedPropertyKey.DependencyProperty;

        [Browsable(false), Category("Appearance"), ReadOnly(true)]
        public bool IsSplitPressed
        {
            get => (bool)GetValue(IsSplitPressedProperty);
            protected set => SetValue(IsSplitPressedPropertyKey, value);
        }

        private void SetIsSplitPressed(bool pressed)
        {
            if (pressed)
            {
                SetValue(IsSplitPressedPropertyKey, true);
            }
            else
            {
                ClearValue(IsSplitPressedPropertyKey);
            }
        }

        protected virtual void OnIsSplitPressedChanged(DependencyPropertyChangedEventArgs e)
        {

        }

        private void UpdateIsSplitPressed()
        {
            if (!UseSplitDropDown)
            {
                if (IsSplitPressed)
                {
                    SetIsSplitPressed(false);
                }

                return;
            }

            Point pos = Mouse.PrimaryDevice.GetPosition(this);

            if ((pos.X >= ActualWidth - splitButtonWidth) && (pos.X <= ActualWidth) && (pos.Y >= 0) && (pos.Y <= ActualHeight))
            {
                if (!IsSplitPressed)
                {
                    SetIsSplitPressed(true);
                }
            }
            else if (IsSplitPressed)
            {
                SetIsSplitPressed(false);
            }
        }

        protected override void OnIsPressedChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnIsPressedChanged(e);
            UpdateIsSplitPressed();
        }

        protected override void OnContextMenuOpening(ContextMenuEventArgs e)
        {
            // do not open the context menu on a right-click
            e.Handled = true;
        }

        protected override void OnClick()
        {
            if (UseSplitDropDown && !IsSplitPressed)
            {
                base.OnClick();
                return;
            }

            if (ContextMenu is ContextMenu contextMenu)
            {
                contextMenu.PlacementTarget = this;
                contextMenu.Placement = PlacementMode.Bottom;
                contextMenu.IsOpen = true;
                contextMenu.Closed += contextMenu_Closed;
                IsPressed = true;
            }
            else
            {
                base.OnClick();
            }
        }

        private void contextMenu_Closed(object sender, System.Windows.RoutedEventArgs e)
        {
            IsPressed = false;

            if (ContextMenu is ContextMenu contextMenu)
            {
                contextMenu.Closed -= contextMenu_Closed;
            }
        }
    }
}
