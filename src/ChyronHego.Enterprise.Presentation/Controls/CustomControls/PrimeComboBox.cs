﻿namespace ChyronHego.Enterprise.Presentation.Controls.CustomControls
{
    using System.Collections;
    using System.Windows.Controls;

    public class PrimeComboBox : ComboBox
    {
        private bool ignoreNextSelectionChangedEvent;

        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            if (ignoreNextSelectionChangedEvent)
            {
                ignoreNextSelectionChangedEvent = false;
                return;
            }

            base.OnSelectionChanged(e);
        }

        protected override void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
        {
            ignoreNextSelectionChangedEvent = newValue != null;
            base.OnItemsSourceChanged(oldValue, newValue);
        }
    }
}