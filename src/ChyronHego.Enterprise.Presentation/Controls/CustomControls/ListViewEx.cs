﻿namespace ChyronHego.Enterprise.Presentation.Controls.CustomControls
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Media;
    using ChyronHego.Enterprise.Presentation.Controls.EventArgs;
    using ChyronHego.Enterprise.Presentation.Extensions;
    using ChyronHego.Enterprise.Presentation.Objects;

    public class ListViewEx : ListView
    {
        public static readonly DependencyProperty ScrollToEndProperty = DependencyProperty.Register(
            "ScrollToEnd", typeof(bool), typeof(ListViewEx));

        [Category("Appearance")]
        [DefaultValue(false)]
        public bool ScrollToEnd
        {
            get { return (bool)GetValue(ScrollToEndProperty); }
            set { SetValue(ScrollToEndProperty, value); }
        }

        public bool EnableColumnsContentMenu
        {
            get; set;
        }

        public ListViewItemSortComparer CustomSort { get; set; }

        public event EventHandler<ColumnSortedEventArgs> ColumnSorted;

        private Dictionary<GridViewColumn, int> columnIndexCollection = new Dictionary<GridViewColumn, int>();
        private ContextMenu ColumnsContextMenu = new ContextMenu();
        private GridViewColumn lastHeaderClickedColumn;
        private ListViewItem mouseUpSelectedItem = null;
        private Point mouseDownPoint;
        private bool ignoreSelection;

        public ListViewEx()
        {
            EnableColumnsContentMenu = false;
            AddHandler(GridViewColumnHeader.ClickEvent, new RoutedEventHandler(GridViewColumnHeaderClickedHandler));
            AddHandler(MouseRightButtonUpEvent, new RoutedEventHandler(GridViewColumnHeaderMouesUp));
            ColumnsContextMenu.ContextMenuOpening += ColumnsContextMenu_ContextMenuOpening;
        }

        static ListViewEx()
        {
            ViewProperty.OverrideMetadata(typeof(ListViewEx), new PropertyMetadata(ViewProperty_Changed));
        }

        public string CopyItems()
        {
            var builder = new StringBuilder();

            var selectedItems = SelectedItems;

            if (View is GridView gridView)
            {
                for (int index = 0; index < gridView.Columns.Count; index++)
                {
                    builder.Append(gridView.Columns[index].Header);
                    builder.Append(index < gridView.Columns.Count - 1 ? "\t" : "\r\n");
                }

                foreach (var item in selectedItems)
                {
                    for (int index = 0; index < gridView.Columns.Count; index++)
                    {
                        var value = gridView.Columns[index].GetBindingValue(item);
                        if (value != null)
                        {
                            builder.Append(value);
                            builder.Append(index < gridView.Columns.Count - 1 ? "\t" : "\r\n");
                        }
                    }
                }
            }

            return builder.ToString();
        }

        private static void ViewProperty_Changed(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            ((ListViewEx)source).ViewChangedEvent();
        }

        protected override void OnItemsChanged(NotifyCollectionChangedEventArgs e)
        {
            base.OnItemsChanged(e);

            if (ScrollToEnd && Items.Count > 0)
            {
                // do not use ScrollIntoView as it takes over 100 ms
                var scrollViewer = GetScrollViewer();

                scrollViewer?.ScrollToBottom();
            }
        }

        protected override void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
        {
            base.OnItemsSourceChanged(oldValue, newValue);

            var oldView = GetListCollectionView(oldValue);
            var newView = GetListCollectionView(newValue);

            if (oldView == null || oldView == newView)
            {
                return;
            }
            newView.CustomSort = oldView.CustomSort;

            newView.SortDescriptions.Clear();
            foreach (var description in oldView.SortDescriptions)
            {
                newView.SortDescriptions.Add(description);
            }
        }

        public bool IsGridView
        {
            get
            {
                return View is GridView;
            }
        }

        public int DefaultColumnWidth { get; set; } = 100;

        private void ViewChangedEvent()
        {
            columnIndexCollection.Clear();
            if (View is GridView grid)
            {
                PopulateColumns(grid.Columns);
            }
        }

        private void ChangeDisplayIndexes()
        {
            if (View is GridView grid)
            {
                int index = 0;
                foreach (var column in grid.Columns)
                {
                    columnIndexCollection[column] = index++;
                }

                var hiddenColumns = columnIndexCollection.Keys.Where(column => !grid.Columns.Contains(column)).ToList();
                foreach (var column in hiddenColumns)
                {
                    columnIndexCollection[column] = index++;
                }
            }
        }

        private void ColumnsContextMenu_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            ColumnsContextMenu.Items.Clear();

            if (View is GridView grid)
            {
                foreach (var column in columnIndexCollection.Keys)
                {
                    bool visible = grid.Columns.Contains(column);
                    ColumnsContextMenu.Items.Add(CreateColumnContextMenuItem(column, visible));
                }
            }
        }
        private void ColumnsContextMenuItem_Clicked(object sender, RoutedEventArgs e)
        {
            if (sender is MenuItem item && this.View is GridView grid)
            {
                if (item.Tag is GridViewColumn column)
                {
                    ShowHideColumn(column, item.IsChecked);
                }
            }
        }
        private MenuItem CreateColumnContextMenuItem(GridViewColumn column, bool visible)
        {
            var menuItem = new MenuItem();
            menuItem.IsCheckable = true;
            menuItem.Header = column.Header.ToString();
            menuItem.IsChecked = visible;
            menuItem.Tag = column;
            if (column is GridViewColumnEx colExtend)
            {
                menuItem.IsEnabled = colExtend.CanHide;
            }
            menuItem.Click += ColumnsContextMenuItem_Clicked;
            return menuItem;
        }
        private Binding FindBindedTextBlock(DependencyObject obj, string headerName = null)
        {
            if (obj == null)
            {
                return null;
            }

            if (obj is Panel panel)
            {
                foreach (var child in panel.Children)
                {
                    var binding = FindBindedTextBlock(child as DependencyObject, headerName);
                    if (binding != null)
                    {
                        if (!string.IsNullOrEmpty(headerName) && (binding.Path.Path == headerName))
                        {
                            return binding;
                        }
                        else
                        {
                            return binding;
                        }
                    }
                }
            }
            else
            {
                return BindingOperations.GetBinding(obj, TextBox.TextProperty);
            }

            return null;
        }

        private string GetSortProperty(GridViewColumn column)
        {
            var bindingBase = column?.DisplayMemberBinding;
            if (bindingBase != null)
            {
                return ((Binding)bindingBase).Path.Path;
            }
            else if (column is GridViewColumnEx columnEx && !string.IsNullOrEmpty(columnEx.BindingPath))
            {
                return columnEx.BindingPath;
            }
            else
            {

                var cellTemplate = column?.CellTemplate;

                if (cellTemplate != null)
                {
                    var binding = FindBindedTextBlock(cellTemplate.LoadContent(), column.Header.ToString());

                    if (binding == null)
                    {
                        binding = FindBindedTextBlock(cellTemplate.LoadContent());
                    }

                    return binding?.Path?.Path;
                }
            }

            return null;
        }

        public ListViewItem GetListViewItem(object source)
        {
            return ControlExtensions.GetTopLevelControl<ListViewItem>(source);
        }

        public ScrollViewer GetScrollViewer()
        {
            return ControlExtensions.FindChild<ScrollViewer>(this);
        }

        public ListCollectionView GetListCollectionView(IEnumerable itemsSource = null)
        {
            return CollectionViewSource.GetDefaultView(itemsSource ?? ItemsSource ?? Items) as ListCollectionView;
        }

        private void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            if (e.OriginalSource is GridViewColumnHeader clickedHeader)
            {
                var dataView = GetListCollectionView();
                if (clickedHeader.Role != GridViewColumnHeaderRole.Padding)
                {

                    string sortProperty = GetSortProperty(clickedHeader?.Column);
                    var direction = GetCurrentSortDirection(sortProperty, dataView);
                    SortColumn(clickedHeader?.Column, dataView, sortProperty, direction);
                }
            }
        }

        public void SortColumn(GridViewColumn column, ListSortDirection direction)
        {
            var dataView = GetListCollectionView();
            string sortProperty = GetSortProperty(column);
            SortColumn(column, dataView, sortProperty, direction);
        }

        public void SortOnProperty(string sortProperty, ListSortDirection sortDirection, bool clearOldSort)
        {
            var dataView = GetListCollectionView();
            Sort(sortProperty, dataView, sortDirection, clearOldSort);
        }

        private void SortColumn(GridViewColumn column, ListCollectionView dataView, string sortProperty, ListSortDirection sortDirection)
        {
            if (sortProperty != null && dataView != null)
            {
                var direction = Sort(sortProperty, dataView, sortDirection);

                if (direction == ListSortDirection.Ascending)
                {
                    column.HeaderTemplate = FindResource("HeaderTemplateArrowUp") as DataTemplate;
                }
                else
                {
                    column.HeaderTemplate = FindResource("HeaderTemplateArrowDown") as DataTemplate;
                }

                if (lastHeaderClickedColumn != null && lastHeaderClickedColumn != column)
                {
                    lastHeaderClickedColumn.HeaderTemplate = null;
                }

                lastHeaderClickedColumn = column;

                RaiseColumnSortedEvent(column, direction);
            }
        }

        private void RaiseColumnSortedEvent(GridViewColumn column, ListSortDirection direction)
        {
            ColumnSorted?.Invoke(this, new ColumnSortedEventArgs(column, direction));
        }

        private void GridViewColumnHeaderMouesUp(object sender, RoutedEventArgs e)
        {
            if (EnableColumnsContentMenu && IsColumnHeaderClicked(e.OriginalSource))
            {
                ColumnsContextMenu.IsOpen = true;
            }
        }

        internal bool IsColumnHeaderClicked(object source)
        {
            return ControlExtensions.GetTopLevelControl<GridViewColumnHeader>(source) != null;
        }

        public void SortColumnsByName(string sortColumnName, ListSortDirection listSortDirection)
        {
            if (View is GridView grid)
            {
                var column = columnIndexCollection.Keys.FirstOrDefault(col => col.Header.ToString() == sortColumnName);
                if (column != null)
                {
                    SortColumn(column, listSortDirection);
                }
            }
        }

        private SortDescriptionCollection GetSortDescriptions(ListCollectionView dataView)
        {
            var descriptions = dataView.SortDescriptions;
            if (CustomSort != null)
            {
                descriptions = CustomSort.SortDescriptions;
            }

            return descriptions;
        }

        private ListSortDirection Sort(string sortProperty, ListCollectionView dataView, ListSortDirection direction, bool clearOldSort = true)
        {
            var sortDescrtipon = GetSortDescriptions(dataView);
            if (clearOldSort)
            {
                sortDescrtipon.Clear();
            }

            var sD = new SortDescription(sortProperty, direction);
            sortDescrtipon.Add(sD);
            if (CustomSort != null)
            {
                dataView.CustomSort = CustomSort;
            }

            dataView.Refresh();
            return direction;
        }

        private ListSortDirection GetCurrentSortDirection(string sortProperty, ListCollectionView dataView)
        {
            var direction = ListSortDirection.Ascending;
            var descriptions = GetSortDescriptions(dataView);
            if (descriptions.Count > 0)
            {
                var currentSort = descriptions[0];
                if (currentSort.PropertyName == sortProperty)
                {
                    if (currentSort.Direction == ListSortDirection.Ascending)
                        direction = ListSortDirection.Descending;
                    else
                        direction = ListSortDirection.Ascending;
                }
            }

            return direction;
        }

        private void PopulateColumns(IList columns)
        {
            if (View is GridView grid)
            {
                grid.Columns.CollectionChanged -= GridViewColumns_CollectionChanged;

                if (columns != null)
                {
                    var toHide = new List<GridViewColumn>();
                    foreach (GridViewColumn column in columns)
                    {
                        if (column.Width == 0)
                        {
                            column.Width = DefaultColumnWidth;
                        }

                        if (!columnIndexCollection.ContainsKey(column))
                        {
                            columnIndexCollection.Add(column, columnIndexCollection.Count);
                        }

                        if (column is GridViewColumnEx columnEx)
                        {
                            AddColumnVisibilityChangedHandler(columnEx);

                            if (!columnEx.Visible)
                            {
                                toHide.Add(column);
                            }
                        }
                    }

                    Dispatcher.InvokeAsync(() =>
                    {
                        foreach (var column in toHide)
                        {
                            grid.Columns.Remove(column);
                        }

                        grid.Columns.CollectionChanged += GridViewColumns_CollectionChanged;
                    });
                }
            }
        }

        public void DeferSelectionForMultiSelectMouseDown(MouseButtonEventArgs e)
        {
            mouseUpSelectedItem = null;
            var item = GetListViewItem(e.OriginalSource);
            if (item?.IsSelected == true
                && SelectedItems.Count > 1
                && !Keyboard.Modifiers.HasFlag(ModifierKeys.Control)
                && !Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
            {
                mouseUpSelectedItem = item;
                mouseDownPoint = e.GetPosition(null);
                e.Handled = true;
            }
        }

        public bool IsMouseDrag(Point currentPoint, Point previousPoint)
        {
            return Math.Abs(currentPoint.X - previousPoint.X) > SystemParameters.MinimumHorizontalDragDistance
                   || Math.Abs(currentPoint.Y - previousPoint.Y) > SystemParameters.MinimumVerticalDragDistance;
        }

        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {
            if (mouseUpSelectedItem != null && IsMouseDrag(e.GetPosition(null), mouseDownPoint))
            {
                mouseUpSelectedItem = null;
            }
        }

        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            if (ignoreSelection)
            {
                e.Handled = true;
            }

            base.OnSelectionChanged(e);
        }

        protected override void OnPreviewMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            if (mouseUpSelectedItem != null)
            {
                ignoreSelection = true;
                SelectedItems.Clear();
                ignoreSelection = false;
                mouseUpSelectedItem.IsSelected = true;
                SetSelectedItems(new List<ListViewItem>() { mouseUpSelectedItem });
                mouseUpSelectedItem = null;

            }

            base.OnPreviewMouseLeftButtonUp(e);
        }

        private void AddColumnVisibilityChangedHandler(GridViewColumnEx columnEx)
        {
            var columnVisibilityChangedHandler = DependencyPropertyDescriptor.FromProperty(GridViewColumnEx.VisibleProperty, typeof(GridViewColumnEx));
            if (columnVisibilityChangedHandler != null)
            {
                columnVisibilityChangedHandler.AddValueChanged(columnEx, ColumnVisibliltyChangedHandler);
            }
        }

        private void ColumnVisibliltyChangedHandler(object sender, EventArgs e)
        {
            if (sender is GridViewColumnEx columnEx)
            {
                ShowHideColumn(columnEx, columnEx.Visible);
            }
        }

        private void ShowHideColumn(GridViewColumn column, bool visible)
        {
            if (View is GridView grid)
            {
                var columnEx = column as GridViewColumnEx;
                grid.Columns.CollectionChanged -= GridViewColumns_CollectionChanged;
                bool containColumn = grid.Columns.Contains(column);
                if (visible)
                {
                    if (!containColumn)
                    {
                        if (!columnIndexCollection.ContainsKey(column))
                        {
                            columnIndexCollection.Add(column, columnIndexCollection.Count);
                        }

                        int index = columnIndexCollection[column];
                        if (index >= 0 && index < grid.Columns.Count - 1)
                        {
                            grid.Columns.Insert(index, column);
                        }
                        else
                        {
                            columnIndexCollection[column] = grid.Columns.Count;
                            grid.Columns.Add(column);
                        }
                    }

                    if (column.Width == 0)
                    {
                        column.Width = DefaultColumnWidth;
                    }
                }
                else if ((columnEx == null || columnEx.CanHide) && containColumn)
                {
                    grid.Columns.Remove(column);
                }

                grid.Columns.CollectionChanged += GridViewColumns_CollectionChanged;
            }
        }

        private void RemoveColumn(IList columns)
        {
            if (columns != null)
            {
                foreach (GridViewColumn column in columns)
                {
                    if (columnIndexCollection.ContainsKey(column))
                    {
                        columnIndexCollection.Remove(column);
                    }
                }

                ResetDisplayIndex();
            }
        }

        private void ResetDisplayIndex()
        {
            var ordered = columnIndexCollection.OrderBy(item => item.Value).ToList();

            int index = 0;
            foreach (var pair in columnIndexCollection.OrderBy(item => item.Value))
            {
                columnIndexCollection[pair.Key] = index++;
            }
        }
        private void GridViewColumns_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                PopulateColumns(e.NewItems);
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                RemoveColumn(e.NewItems);
            }
            else if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                columnIndexCollection.Clear();
                if (e.NewItems != null)
                {
                    PopulateColumns(e.NewItems);
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Move)
            {
                MoveColumns(e.NewItems, e.NewStartingIndex, e.OldStartingIndex);
            }
        }

        public void ScrollToTop()
        {
            var scroll = GetScrollViewer();
            scroll?.ScrollToTop();
        }

        private void MoveColumns(IList columns, int newIndex, int oldIndex)
        {
            if (View is GridView grid)
            {
                var columnIndexPair = columnIndexCollection.FirstOrDefault(pair => pair.Value == newIndex);
                bool processIndexChange = true;


                if (!CanMoveColumn(newIndex) || !CanMoveColumn(oldIndex))
                {
                    MoveColumnAsync(grid, newIndex, oldIndex);

                    processIndexChange = false;
                }

                if (processIndexChange)
                {
                    ChangeDisplayIndexes();
                }
            }
        }

        private bool CanMoveColumn(int index)
        {
            var columnIndexPair = columnIndexCollection.FirstOrDefault(pair => pair.Value == index);
            if (columnIndexPair.Key is GridViewColumnEx columnEx)
            {
                return columnEx.CanReorder;
            }
            return true;
        }

        private void MoveColumnAsync(GridView grid, int oldIndex, int newIndex)
        {
            Dispatcher.InvokeAsync(() =>
            {
                grid.Columns.CollectionChanged -= GridViewColumns_CollectionChanged;
                grid.Columns.Move(oldIndex, newIndex);
                grid.Columns.CollectionChanged += GridViewColumns_CollectionChanged;
            });
        }
    }
}