﻿namespace ChyronHego.Enterprise.Presentation.Controls.CustomControls
{
    using System;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Controls;

    public class GridViewColumnEx : GridViewColumn
    {
        public string Name { get; set; }

        public bool CanReorder { get; }

        public bool CanHide { get; }

        public string BindingPath { get; set; }

        public static readonly DependencyProperty VisibleProperty = DependencyProperty.Register(
            "Visible", typeof(Boolean), typeof(GridViewColumnEx));

        [Category("Appearance")]
        [DefaultValue(true)]
        public bool Visible
        {
            get { return (bool) GetValue(VisibleProperty); }
            set { SetValue(VisibleProperty, value); }
        }

        public GridViewColumnEx(string name, bool canHide, bool canReorder)
        {
            Name = name;
            CanReorder = canReorder;
            CanHide = canHide;
        }
    }
}