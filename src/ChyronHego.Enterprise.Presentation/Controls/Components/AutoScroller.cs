﻿namespace ChyronHego.Enterprise.Presentation.Controls.Components
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Threading;
    using ChyronHego.Enterprise.Presentation.Controls.EventArgs;
    using ChyronHego.Enterprise.Presentation.Extensions;

    public sealed class AutoScroller
    {
        private readonly DispatcherTimer autoScroll = new DispatcherTimer();
        private readonly ItemsControl itemsControl;
        private readonly ScrollContentPresenter scrollContent;
        private readonly ScrollViewer scrollViewer;
        private bool isEnabled;
        private Point mouse;
        private Point offset;
        public AutoScroller(ItemsControl itemsControl)
        {
            if (itemsControl == null)
            {
                throw new ArgumentNullException("itemsControl");
            }

            this.itemsControl = itemsControl;
            scrollViewer = ControlExtensions.FindChild<ScrollViewer>(itemsControl);

            if (scrollViewer != null)
            {
                scrollViewer.ScrollChanged += OnScrollChanged;
                scrollContent = ControlExtensions.FindChild<ScrollContentPresenter>(scrollViewer);

                autoScroll.Tick += delegate { PreformScroll(); };
                autoScroll.Interval = TimeSpan.FromMilliseconds(GetRepeatRate());
            }
        }

        public event EventHandler<OffsetChangedEventArgs> OffsetChanged;

        public bool IsEnabled
        {
            get
            {
                return isEnabled;
            }
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;

                    autoScroll.IsEnabled = false;
                    offset = new Point();
                }
            }
        }

        public Point TranslatePoint(Point point)
        {
            return new Point(point.X - this.offset.X, point.Y - this.offset.Y);
        }

        public void UnRegister()
        {
            scrollViewer.ScrollChanged -= this.OnScrollChanged;
        }

        public void Update(Point mouse)
        {
            this.mouse = mouse;

            if (!autoScroll.IsEnabled && scrollContent != null)
            {
                PreformScroll();
            }
        }

        private static int GetRepeatRate()
        {
            const double Ratio = (400.0 - 33.0) / 31.0;
            return 400 - (int)(SystemParameters.KeyboardSpeed * Ratio);
        }

        private double CalculateOffset(int startIndex, int endIndex)
        {
            double sum = 0;
            for (int i = startIndex; i != endIndex; i++)
            {
                var container = this.itemsControl.ItemContainerGenerator.ContainerFromIndex(i) as FrameworkElement;
                if (container != null)
                {
                    sum += container.ActualHeight;
                    sum += container.Margin.Top + container.Margin.Bottom;
                }
            }
            return sum;
        }

        private void OnScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (this.IsEnabled)
            {
                double horizontal = e.HorizontalChange;
                double vertical = e.VerticalChange;

                if (this.scrollViewer.CanContentScroll)
                {
                    if (e.VerticalChange < 0)
                    {
                        int start = (int)e.VerticalOffset;
                        int end = (int)(e.VerticalOffset - e.VerticalChange);
                        vertical = -this.CalculateOffset(start, end);
                    }
                    else
                    {
                        int start = (int)(e.VerticalOffset - e.VerticalChange);
                        int end = (int)e.VerticalOffset;
                        vertical = this.CalculateOffset(start, end);
                    }
                }

                offset.X += horizontal;
                offset.Y += vertical;

                var callback = OffsetChanged;
                if (callback != null)
                {
                    callback(this, new OffsetChangedEventArgs(horizontal, vertical));
                }
            }
        }

        private void PreformScroll()
        {
            bool scrolled = false;

            if (mouse.X > this.scrollContent.ActualWidth)
            {
                scrollViewer.LineRight();
                scrolled = true;
            }
            else if (this.mouse.X < 0)
            {
                scrollViewer.LineLeft();
                scrolled = true;
            }

            if (mouse.Y > this.scrollContent.ActualHeight)
            {
                scrollViewer.LineDown();
                scrolled = true;
            }
            else if (this.mouse.Y < 0)
            {
                scrollViewer.LineUp();
                scrolled = true;
            }

            autoScroll.IsEnabled = scrolled;
        }
    }
}