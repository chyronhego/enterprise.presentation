﻿namespace ChyronHego.Enterprise.Presentation.Controls.Components
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;

    public sealed class ItemsControlSelector
    {
        private readonly ItemsControl itemsControl;
        private Rect previousArea;

        public ItemsControlSelector(ItemsControl itemsControl)
        {
            this.itemsControl = itemsControl ?? throw new ArgumentNullException("itemsControl");
        }

        public void Reset()
        {
            previousArea = new Rect();
        }

        public void Scroll(double x, double y)
        {
            previousArea.Offset(-x, -y);
        }

        public void UpdateSelection(Rect area)
        {
            for (int i = 0; i < itemsControl.Items.Count; i++)
            {
                var item = itemsControl.ItemContainerGenerator.ContainerFromIndex(i) as FrameworkElement;
                if (item != null)
                {
                    var topLeft = item.TranslatePoint(new Point(0, 0), itemsControl);
                    var itemBounds = new Rect(topLeft.X, topLeft.Y, item.ActualWidth, item.ActualHeight);

                    if (itemBounds.IntersectsWith(area))
                    {
                        Selector.SetIsSelected(item, true);
                    }
                    else if (itemBounds.IntersectsWith(this.previousArea))
                    {
                        Selector.SetIsSelected(item, false);
                    }
                }
            }
            previousArea = area;
        }
    }
}