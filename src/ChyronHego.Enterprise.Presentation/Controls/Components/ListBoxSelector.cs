﻿namespace ChyronHego.Enterprise.Presentation.Controls.Components
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using ChyronHego.Enterprise.Presentation.Controls.CustomControls;
    using ChyronHego.Enterprise.Presentation.Controls.EventArgs;
    using ChyronHego.Enterprise.Presentation.Extensions;

    public sealed class ListBoxSelector
    {
        public static readonly DependencyProperty EnabledProperty =
            DependencyProperty.RegisterAttached("Enabled", typeof(bool), typeof(ListBoxSelector), new UIPropertyMetadata(false, IsEnabledChangedCallback));

        private static readonly Dictionary<ListBox, ListBoxSelector> attachedControls = new Dictionary<ListBox, ListBoxSelector>();

        private readonly ListBox listBox;

        private AutoScroller autoScroller;
        private Point end;
        private bool mouseCaptured;
        private ScrollContentPresenter scrollContent;
        private SelectionAdorner selectionRect;
        private ItemsControlSelector selector;
        private Point start;

        private ListBoxSelector(ListBox control)
        {
            listBox = control;
            if (listBox.IsLoaded)
            {
                Register();
            }
            else
            {
                listBox.Loaded += OnListBoxLoaded;
            }
        }

        public static bool GetEnabled(DependencyObject obj)
        {
            return (bool)obj.GetValue(EnabledProperty);
        }

        public static void SetEnabled(DependencyObject obj, bool value)
        {
            obj.SetValue(EnabledProperty, value);
        }

        private static void IsEnabledChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var listBox = d as ListViewEx;
            if (listBox != null)
            {
                if ((bool)e.NewValue)
                {
                    if (listBox.SelectionMode == SelectionMode.Single)
                    {
                        listBox.SelectionMode = SelectionMode.Extended;
                    }

                    attachedControls.Add(listBox, new ListBoxSelector(listBox));
                }
                else
                {
                    ListBoxSelector selector;
                    if (attachedControls.TryGetValue(listBox, out selector))
                    {
                        attachedControls.Remove(listBox);
                        selector.UnRegister();
                    }
                }
            }
        }

        private void AttachSelectionRectLayer()
        {
            scrollContent = ControlExtensions.FindChild<ScrollContentPresenter>(listBox);
            if (scrollContent != null)
            {
                if (selectionRect == null || !IsSelectionRectAttached())
                {
                    selectionRect = new SelectionAdorner(scrollContent);
                    scrollContent.AdornerLayer.Add(selectionRect);
                }
            }
        }

        private bool IsSelectionRectAttached()
        {
            var adornerLayers = scrollContent.AdornerLayer.GetAdorners(scrollContent);

            if (adornerLayers != null)
            {
                return adornerLayers.Contains(selectionRect);
            }

            return false;
        }

        private void OnListBoxLoaded(object sender, EventArgs e)
        {
            if (Register())
            {
                listBox.Loaded -= OnListBoxLoaded;
            }
        }

        private void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (mouseCaptured)
            {
                mouseCaptured = false;
                scrollContent.ReleaseMouseCapture();
                StopSelection();
            }
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (mouseCaptured)
            {
                end = e.GetPosition(scrollContent);
                autoScroller.Update(end);
                UpdateSelection();
            }
        }

        private void OnOffsetChanged(object sender, OffsetChangedEventArgs e)
        {
            selector.Scroll(e.HorizontalChange, e.VerticalChange);
            UpdateSelection();
        }

        private void OnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            AttachSelectionRectLayer();

            var mouse = e.GetPosition(scrollContent);
            if ((mouse.X >= 0) && (mouse.X < scrollContent.ActualWidth) &&
                (mouse.Y >= 0) && (mouse.Y < scrollContent.ActualHeight))
            {
                mouseCaptured = TryCaptureMouse(e);
                if (mouseCaptured)
                {
                    StartSelection(mouse);
                }
            }
        }

        private bool Register()
        {
            if (listBox != null)
            {
                autoScroller = new AutoScroller(listBox);
                autoScroller.OffsetChanged += OnOffsetChanged;

                selector = new ItemsControlSelector(listBox);

                listBox.PreviewMouseLeftButtonDown += OnPreviewMouseLeftButtonDown;
                listBox.MouseLeftButtonUp += OnMouseLeftButtonUp;
                listBox.MouseMove += OnMouseMove;
            }

            return true;
        }

        private void StartSelection(Point location)
        {
            listBox.Focus();

            start = location;
            end = location;

            if (((Keyboard.Modifiers & ModifierKeys.Control) == 0) &&
                ((Keyboard.Modifiers & ModifierKeys.Shift) == 0))
            {
                listBox.SelectedItems.Clear();
            }

            selector.Reset();
            UpdateSelection();

            selectionRect.IsEnabled = true;
            autoScroller.IsEnabled = true;
        }

        private void StopSelection()
        {
            selectionRect.IsEnabled = false;
            autoScroller.IsEnabled = false;
        }

        private bool TryCaptureMouse(MouseButtonEventArgs e)
        {
            var position = e.GetPosition(scrollContent);

            var element = scrollContent.InputHitTest(position) as UIElement;
            if (element != null)
            {
                return false;
            }

            return scrollContent.CaptureMouse();
        }

        private void UnRegister()
        {
            StopSelection();

            listBox.PreviewMouseLeftButtonDown -= OnPreviewMouseLeftButtonDown;
            listBox.MouseLeftButtonUp -= OnMouseLeftButtonUp;
            listBox.MouseMove -= OnMouseMove;

            autoScroller.UnRegister();
        }

        private void UpdateSelection()
        {
            var newStart = autoScroller.TranslatePoint(start);

            double x = Math.Min(newStart.X, end.X);
            double y = Math.Min(newStart.Y, end.Y);
            double width = Math.Abs(end.X - newStart.X);
            double height = Math.Abs(end.Y - newStart.Y);
            var area = new Rect(x, y, width, height);
            selectionRect.SelectionArea = area;

            var topLeft = scrollContent.TranslatePoint(area.TopLeft, listBox);
            var bottomRight = scrollContent.TranslatePoint(area.BottomRight, listBox);

            selector.UpdateSelection(new Rect(topLeft, bottomRight));
        }
    }
}