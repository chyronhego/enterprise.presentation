﻿namespace ChyronHego.Enterprise.Presentation.Controls.Components
{
    using System;
    using System.Windows;
    using System.Windows.Documents;
    using System.Windows.Media;

    public sealed class SelectionAdorner : Adorner
    {
        private Rect selectionRect;

        public SelectionAdorner(UIElement parent)
            : base(parent)
        {
           IsHitTestVisible = false;

            IsEnabledChanged += delegate { InvalidateVisual(); };
        }

        public Rect SelectionArea
        {
            get
            {
                return selectionRect;
            }
            set
            {
                selectionRect = value;
                InvalidateVisual();
            }
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (IsEnabled)
            {
                double[] x = { this.SelectionArea.Left + 0.5, this.SelectionArea.Right + 0.5 };
                double[] y = { this.SelectionArea.Top + 0.5, this.SelectionArea.Bottom + 0.5 };
                drawingContext.PushGuidelineSet(new GuidelineSet(x, y));

                Brush fill = SystemColors.HighlightBrush.Clone();
                fill.Opacity = 0.4;
                drawingContext.DrawRectangle(fill, new Pen(SystemColors.HighlightBrush, 1.0), this.SelectionArea);
            }
        }
    }
}