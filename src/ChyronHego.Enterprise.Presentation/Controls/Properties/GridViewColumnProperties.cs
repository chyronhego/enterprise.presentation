﻿namespace ChyronHego.Enterprise.Presentation.Controls.Properties
{
    using System.Windows;
    using System.Windows.Controls;

    public class GridViewColumnProperties
    {
        public static readonly DependencyProperty NameProperty =
            DependencyProperty.RegisterAttached("Name", typeof(string), typeof(GridViewColumn), new FrameworkPropertyMetadata(""));

        public static void SetName(UIElement element, string value)
        {
            element.SetValue(NameProperty, value);
        }

        public static string GetName(UIElement element)
        {
            return (string) element.GetValue(NameProperty);
        }
    }
}