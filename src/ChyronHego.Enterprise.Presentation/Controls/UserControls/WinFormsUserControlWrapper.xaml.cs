﻿namespace ChyronHego.Enterprise.Presentation.Controls.UserControls
{
    using System.Windows.Forms;
    using WpfUserControl = System.Windows.Controls.UserControl;
    using WinFormsUserControl = System.Windows.Forms.UserControl;

    /// <summary>
    /// Interaction logic for WinFormsUserControlWrapper.xaml
    /// </summary>
    public partial class WinFormsUserControlWrapper : WpfUserControl
    {
        public WinFormsUserControlWrapper()
        {
            InitializeComponent();
        }

        public WinFormsUserControlWrapper(WinFormsUserControl winFormsUserControl)
        {
            InitializeComponent();
            WinFormsUserControl = winFormsUserControl;
            if (WinFormsUserControl != null)
            {
                WinFormsUserControl.Dock = DockStyle.Fill;
                host.Child = WinFormsUserControl;
            }
        }

        public WinFormsUserControl WinFormsUserControl { get; }
    }
}
