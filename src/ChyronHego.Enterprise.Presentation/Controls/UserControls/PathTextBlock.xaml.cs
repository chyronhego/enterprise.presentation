﻿namespace ChyronHego.Enterprise.Presentation.Controls.UserControls
{
    using System.Globalization;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using ChyronHego.Enterprise.Infrastructure.Extensions;
    using IOPath = System.IO.Path;

    public partial class PathTextBlock : UserControl
    {
        private const string Ellipsis = "...";
        private const string DirectorySeparator = "\\";
        private const int WidthPadding = 5;

        public static readonly DependencyProperty PathProperty =
            DependencyProperty.Register(nameof(Path), typeof(string), typeof(PathTextBlock), new UIPropertyMetadata("", PathProperty_Changed));

        public static readonly DependencyProperty TextStyleProperty =
            DependencyProperty.Register(nameof(TextStyle), typeof(Style), typeof(PathTextBlock), new UIPropertyMetadata(null));

        public PathTextBlock()
        {
            InitializeComponent();
        }

        private static void PathProperty_Changed(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            ((PathTextBlock)source).InvalidateMeasure();
        }

        public string Path
        {
            get => (string)GetValue(PathProperty);
            set => SetValue(PathProperty, value);
        }
        public Style TextStyle
        {
            get => (Style)GetValue(TextStyleProperty);
            set => SetValue(TextStyleProperty, value);
        }

        protected override Size MeasureOverride(Size constraint)
        {
            base.MeasureOverride(constraint);

            constraint.Width -= WidthPadding;

            var trimResult = TrimToFit(constraint);

            textBlock.Text = trimResult.Text;

            return trimResult.Size;
        }

        private Size MeasureString(Typeface typeface, string text)
        {
            var dpi = VisualTreeHelper.GetDpi(textBlock);

            var formattedText = new FormattedText(
                    text ?? string.Empty,
                    CultureInfo.CurrentCulture,
                    FlowDirection.LeftToRight,
                    typeface,
                    textBlock.FontSize,
                    textBlock.Foreground,
                    dpi.PixelsPerDip
                );
            formattedText.TextAlignment = textBlock.TextAlignment;

            return new Size(formattedText.WidthIncludingTrailingWhitespace, formattedText.Height);
        }

        private (Size Size, string Text) TrimToFit(Size constraint)
        {
            var typeface = new Typeface(textBlock.FontFamily, textBlock.FontStyle, textBlock.FontWeight, textBlock.FontStretch);

            var size = MeasureString(typeface, Path);
            if (Path.IsNullOrEmpty() || DoesTextFit(size, constraint))
            {
                return (size, Path);
            }

            string filename = IOPath.GetFileName(Path);
            string directory = IOPath.GetDirectoryName(Path);

            bool trimmedFilename = false;

            while (true)
            {
                string separator = trimmedFilename ? string.Empty : DirectorySeparator;
                string text = $"{directory}{Ellipsis}{separator}{filename}";

                size = MeasureString(typeface, text);

                if (DoesTextFit(size, constraint))
                {
                    return (size, text);
                }

                if (directory.Length > 0 || filename.Length > 0)
                {
                    if (directory.Length == 0)
                    {
                        trimmedFilename = true;
                        filename = filename.Substring(1);
                    }
                    else
                    {
                        directory = directory.Substring(0, directory.Length - 1);
                    }
                }
                else
                {
                    return (Size.Empty, string.Empty);
                }
            }
        }

        private bool DoesTextFit(Size textSize, Size constraint)
        {
            return textSize.Width < constraint.Width;
        }
    }
}
