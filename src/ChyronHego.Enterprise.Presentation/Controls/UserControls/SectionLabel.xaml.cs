﻿namespace ChyronHego.Enterprise.Presentation.Controls.UserControls
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for SectionLabel.xaml
    /// </summary>
    public partial class SectionLabel : UserControl
    {
        public SectionLabel()
        {
            InitializeComponent();
        }
    }
}
