﻿namespace ChyronHego.Enterprise.Presentation.Controls.UserControls
{
    using System.Windows.Controls;
    using System.Windows.Media;

    public partial class DragGrip : UserControl
    {
        private readonly static Brush DefaultBrush = new SolidColorBrush(Color.FromRgb(187, 187, 187));

        public DragGrip()
        {
            Foreground = DefaultBrush.Clone();
            InitializeComponent();
        }

        protected override HitTestResult HitTestCore(PointHitTestParameters parameters)
        {
            // makes sure the entire bounding rectangle can be clicked
            // without this only the painted pixels can be clicked

            return new PointHitTestResult(this, parameters.HitPoint);
        }

        private void DragGrip_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // allows dragging the mouse after clicking this button
            e.Handled = true;
            CaptureMouse();
        }

        private void DragGrip_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // allows dragging the mouse after clicking this button
            e.Handled = true;
            ReleaseMouseCapture();
        }
    }
}
