﻿namespace ChyronHego.Enterprise.Presentation.Controls.Interfaces
{
    using ChyronHego.Enterprise.Presentation.Controls.Enums;

    public interface IReorderableRow
    {
        ReorderDropState ReorderDropState { get;set; }
    }
}
