﻿namespace ChyronHego.Enterprise.Presentation.Objects
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using ChyronHego.Enterprise.Presentation.Extensions;

    public class ListViewItemSortComparer : IComparer
    {
        public SortDescriptionCollection SortDescriptions { get; private set; } = new SortDescriptionCollection();

        protected virtual IEnumerable<SortDescription> EnumerateSortDescriptions
        {
            get
            {
                foreach (var description in SortDescriptions)
                {
                    yield return description;
                }
            }
        }

        public int Compare(object x, object y)
        {
            int returnValue = 0;

            if (x == null && y == null)
            {
                return returnValue;
            }
            else if (x == null)
            {
                return -1;
            }
            else if (y == null)
            {
                return 1;
            }

            foreach (var description in EnumerateSortDescriptions)
            {
                string propertyName = description.PropertyName;
                var columnValue1 = ControlExtensions.GetPropValue(x, propertyName);
                var columnValue2 = ControlExtensions.GetPropValue(y, propertyName);

                if (description.Direction == ListSortDirection.Ascending)
                {
                    returnValue = CompareValues(columnValue1, columnValue2, propertyName);
                }
                else
                {
                    returnValue = CompareValues(columnValue2, columnValue1, propertyName);
                }

                if (returnValue != 0)
                {
                    break;
                }
            }
            return returnValue;
        }

        public virtual int CompareValues(object x, object y, string propertyName)
        {
            return DefaultComparer(x, y);
        }

        public int DefaultComparer(object x, object y)
        {
            if (x is IComparable obj1 && y is IComparable obj2)
            {
                return obj1.CompareTo(obj2);
            }

            return 0;
        }
    }
}