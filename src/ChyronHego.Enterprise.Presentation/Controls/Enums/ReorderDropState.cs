﻿namespace ChyronHego.Enterprise.Presentation.Controls.Enums
{
    public enum ReorderDropState
    {
        None,
        Above,
        Below
    }
}
