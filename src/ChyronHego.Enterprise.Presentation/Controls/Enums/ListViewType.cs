﻿namespace ChyronHego.Enterprise.Presentation.Controls.Enums
{
    public enum ListViewType
    {
        Details,
        List,
        Icon
    }
}