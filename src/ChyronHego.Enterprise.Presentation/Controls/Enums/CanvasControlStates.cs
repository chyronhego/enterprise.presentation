﻿namespace ChyronHego.Prime.Editor.SplineEditor.ViewModels
{
    public enum CanvasControlStates
    {
        Pan,
        Select
    }
}