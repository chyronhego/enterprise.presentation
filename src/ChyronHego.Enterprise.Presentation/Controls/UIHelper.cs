﻿namespace ChyronHego.Enterprise.Presentation.Controls
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Media;

    internal static class UIHelper
    {
        private const string SelectedItemsPropertyKey = "SelectedItems";

        internal static UIElement GetUIElement(ItemsControl container, Point position)
        {
            if (container.InputHitTest(position) is UIElement elementAtPosition)
            {
                while (elementAtPosition != null)
                {
                    var testUiElement = container.ItemContainerGenerator.ItemFromContainer(elementAtPosition);

                    if (testUiElement != DependencyProperty.UnsetValue)
                    {
                        return elementAtPosition;
                    }

                    elementAtPosition = VisualTreeHelper.GetParent(elementAtPosition) as UIElement;
                }
            }

            for (int i = 0; i < container.Items.Count; i++)
            {
                if (!(container.ItemContainerGenerator.ContainerFromIndex(i) is FrameworkElement itemContainer))
                {
                    return null;
                }

                if (position.Y > itemContainer.ActualHeight || position.Y < 0)
                {
                    return itemContainer;
                }
            }

            return null;
        }

        internal static bool IsPositionAboveElement(UIElement element, Point relativePosition)
        {
            if (element is FrameworkElement frameworkElement)
            {
                if (relativePosition.Y < frameworkElement.ActualHeight / 2)
                {
                    return true;
                }
            }

            return false;
        }

        public static T FindParentOfType<T>(DependencyObject element) where T : DependencyObject
        {
            while (true)
            {
                var desiredParent = VisualTreeHelper.GetParent(element);

                if (desiredParent is T parent)
                {
                    return parent;
                }
                else if (desiredParent != null)
                {
                    element = desiredParent;
                    continue;
                }

                return null;
            }
        }

        public static IEnumerable<object> GetSelectedItemsFromContainer<T>(FrameworkElement element) where T : DependencyObject
        {
            if (!(FindParentOfType<T>(element) is T parent))
            {
                yield break;
            }

            if (!(parent is Selector selector))
            {
                yield break;
            }

            var type = selector.GetType();
            var selectedItems = type.GetProperty(SelectedItemsPropertyKey);

            if (selectedItems == null)
            {
                yield break;
            }

            foreach (var selectedItem in (IEnumerable)selectedItems.GetValue(selector))
            {
                yield return selectedItem;
            }
        }
    }
}
