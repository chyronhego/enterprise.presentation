﻿namespace ChyronHego.Enterprise.Presentation.Controls
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Input;

    public class ContextMenuButton : Button
    {
        public ContextMenuButton()
        {
            if (TryFindResource(ToolBar.ButtonStyleKey) is Style toolBarButtonStyle)
            {
                var style = new Style(typeof(ContextMenuButton), toolBarButtonStyle);

                Resources.Add(typeof(ContextMenuButton), style);
            }
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.Property == ContextMenuProperty)
            {
                if (e.OldValue is ContextMenu contextMenu)
                {
                    contextMenu.Closed -= ContextMenu_Closed;
                }

                if (ContextMenu != null)
                {
                    ContextMenu.PlacementTarget = this;

                    ContextMenu.Closed += ContextMenu_Closed;
                }
            }
        }

        private void ContextMenu_Closed(object sender, RoutedEventArgs e)
        {
            IsPressed = false;
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);

            if (ContextMenu != null && ContextMenu.HasItems)
            {
                ContextMenu.Placement = PlacementMode.Bottom;
                ContextMenu.IsOpen = true;

                IsPressed = true;

                e.Handled = true;
            }
        }
    }
}
