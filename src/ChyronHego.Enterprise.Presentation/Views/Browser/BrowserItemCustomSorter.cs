﻿namespace ChyronHego.Enterprise.Presentation.ViewModels
{
    using System.Collections.Generic;
    using System.Linq;
    using ChyronHego.Enterprise.Presentation.Objects;
    using System.ComponentModel;

    public class BrowserItemCustomSorter : ListViewItemSortComparer
    {
        private BrowserViewModel browserViewModel;

        public BrowserItemCustomSorter(BrowserViewModel viewModel)
        {
            browserViewModel = viewModel;
        }

        protected override IEnumerable<SortDescription> EnumerateSortDescriptions
        {
            get
            {
                var direction = (SortDescriptions.Count > 0)
                    && (SortDescriptions[0].Direction == ListSortDirection.Descending)
                    ? ListSortDirection.Ascending : ListSortDirection.Descending;

                yield return new SortDescription(nameof(BrowserItem.IsDirectory), direction);

                foreach (var description in SortDescriptions)
                {
                    yield return description;
                }
            }
        }

        public override int CompareValues(object x, object y, string propertyName)
        {
            var compare = browserViewModel.Columns.FirstOrDefault(col => col.BindingPath == propertyName)?.Compare;
            if (compare == null)
            {
                compare = DefaultComparer;
            }

            return compare(x, y);
        }
    }
}