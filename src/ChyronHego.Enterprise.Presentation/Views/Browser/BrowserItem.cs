﻿namespace ChyronHego.Enterprise.Presentation.Objects
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Media.Imaging;
    using ChyronHego.Enterprise.Presentation.Collections;

    public class BrowserItem : INotifyPropertyChanged
    {
        private BitmapImage thumbnail;
        private string name;
        private string toolTipText;
        private ObservableCollectionEx<string> properties;
        private bool renaming;
        private string qualityControl;
        private bool editingQuality;
        private bool isDirectory;
        private string camioChannel;
        private bool editingCamioChannel;

        public BitmapImage Thumbnail
        {
            get
            {
                if (thumbnail == null)
                {
                    FetchThumbnailEvent?.Invoke(this, EventArgs.Empty);
                }

                return thumbnail;
            }
            set
            {
                if (thumbnail != value)
                {                    
                    thumbnail = value;

                    OnPropertyChanged("Thumbnail");
                }
            }
        }

        public string Name
        {
            get => name;
            set
            {
                if (name != value)
                {
                    name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        public string QualityControl
        {
            get => qualityControl;
            set
            {
                if (qualityControl != value)
                {
                    qualityControl = value;
                    OnPropertyChanged("QualityControl");
                }
            }
        }

        public string CamioChannel
        {
            get => camioChannel;
            set
            {
                if (camioChannel != value)
                {
                    camioChannel = value;
                    OnPropertyChanged("CamioChannel");
                }
            }
        }

        public bool Renaming
        {
            get
            {
                return renaming;
            }

            set
            {
                if (renaming != value)
                {
                    renaming = value;
                    OnPropertyChanged("Renaming");
                }
            }
        }

        public bool EditingCamioChannel
        {
            get
            {
                return editingCamioChannel;
            }

            set
            {
                if (editingCamioChannel != value)
                {
                    editingCamioChannel = value;
                    OnPropertyChanged("EditingCamioChannel");
                }
            }
        }

        public bool IsDirectory
        {
            get
            {
                return isDirectory;
            }

            set
            {
                if (isDirectory != value)
                {
                    isDirectory = value;
                    OnPropertyChanged("IsDirectory");
                }
            }
        }

        public bool EditingQuality
        {
            get
            {
                return editingQuality;
            }

            set
            {
                if (editingQuality != value && !isDirectory)
                {
                    editingQuality = value;
                    OnPropertyChanged("EditingQuality");
                }
            }
        }

        public ObservableCollectionEx<string> Properties
        {
            get => properties;
            set
            {
                if (properties != value)
                {
                    properties = value;
                    OnPropertyChanged("Properties");
                }

            }
        }

        public object Tag { get; set; }        

        public string ToolTipText
        {
            get => toolTipText;
            set
            {
                if (toolTipText != value)
                {
                    toolTipText = value;
                    OnPropertyChanged("ToolTipText");
                }
            }
        }

        public BrowserItem(BitmapImage thumbnail, string name, IEnumerable<string> propertiesCollection)
        {
            Thumbnail = thumbnail;
            Name = name;
            Properties = new ObservableCollectionEx<string>(propertiesCollection);
            editingQuality = false;
        }

        public BrowserItem()
        {
            Properties = new ObservableCollectionEx<string>();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        public event EventHandler FetchThumbnailEvent;
    }
}