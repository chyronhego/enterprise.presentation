﻿namespace ChyronHego.Enterprise.Presentation.Objects
{
    using System.ComponentModel;
    using System.Windows.Media;

    public class QualityControlInfo : INotifyPropertyChanged
    {
        private Brush background;
        private Brush foreground;
        private string name;

        public event PropertyChangedEventHandler PropertyChanged;

        public Brush Background
        {
            get
            {
                return background;
            }
            set
            {
                if (background != value)
                {
                    background = value;
                    OnPropertyChanged("Backcolor");
                }
            }
        }

        public Brush Foreground
        {
            get
            {
                return foreground;
            }
            set
            {
                if (foreground != value)
                {
                    foreground = value;
                    OnPropertyChanged("ForeColor");
                }
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (name != value)
                {
                    name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        private void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}