namespace ChyronHego.Enterprise.Presentation.Views
{
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using ChyronHego.Enterprise.Presentation.Controls.Properties;
    using ChyronHego.Enterprise.Presentation.Controls.CustomControls;
    using ChyronHego.Enterprise.Presentation.Controls.Enums;
    using ChyronHego.Enterprise.Presentation.Extensions;
    using ChyronHego.Enterprise.Presentation.Objects;
    using ChyronHego.Enterprise.Presentation.Styles.Properties;
    using ChyronHego.Enterprise.Presentation.ViewModels;

    /// <summary>
    /// Interaction logic for PrimeBrowserView.xaml
    /// </summary>
    public partial class BrowserView : BaseView
    {
        public static readonly DependencyProperty ThumbnailScaleProperty = DependencyProperty.Register(
           "ThumbnailScale", typeof(Size), typeof(BrowserView));

        public static readonly DependencyProperty ViewTypeProperty = DependencyProperty.Register(
           "ViewType", typeof(ListViewType), typeof(BrowserView));

        public static readonly DependencyProperty ColumnHeaderContextMenuProperty = DependencyProperty.Register(
            nameof(ColumnHeaderContextMenu),
            typeof(ContextMenu),
            typeof(BrowserView),
            new PropertyMetadata(ColumnHeaderContextMenuPropertyChanged));

        private Style defaultStyle;
        private TileView iconView;
        private TileView listView;
        private GridView gridView;
        private Style tileListViewStyle;

        public BrowserView()
        {
            InitializeComponent();
            ViewType = ListViewType.Details;
            CreateListStyle();
            ThumbnailScale = new Size(100, 100);
            DataContextChanged += BrowserView_DataContextChanged;
            AttachViewModel();
        }

        public BrowserView(BrowserViewModel model) : this()
        {
            DataContext = model;
        }

        public GridView GridView
        {
            get => gridView;
            private set
            {
                gridView = value;
                if (value != null)
                {
                    value.ColumnHeaderContextMenu = ColumnHeaderContextMenu;
                }
            }
        }

        public ContextMenu ColumnHeaderContextMenu
        {
            get => GetValue(ColumnHeaderContextMenuProperty) as ContextMenu;
            set => SetValue(ColumnHeaderContextMenuProperty, value);
        }

        public ListViewStyleManager ListViewStyleManager { get; private set; }

        public int PageDisplayItemCount
        {
            get
            {
                if (ThumbnailScale.Height == 0)
                {
                    return 0;
                }

                if (ViewType == ListViewType.Details)
                {
                    return (int)(ListView.ActualHeight / ThumbnailScale.Height);
                }
                else if (ListView.View is TileView tileView)
                {
                    int rows = (int)(ListView.ActualHeight / tileView.ItemHeight);
                    int columns = (int)(ListView.ActualWidth / tileView.ItemWidth);
                    return rows * columns;
                }

                return 0;
            }
        }

        public Size ThumbnailScale
        {
            get { return (Size)GetValue(ThumbnailScaleProperty); }
            set { SetValue(ThumbnailScaleProperty, value); }
        }

        public ListViewType ViewType
        {
            get { return (ListViewType)GetValue(ViewTypeProperty); }
            set
            {
                SetValue(ViewTypeProperty, value);
                SwitchListView();
            }
        }

        private BrowserViewModel BrowserViewModel
        {
            get { return DataContext as BrowserViewModel; }
        }

        private void AttachViewModel()
        {
            BrowserViewModel.SelectedItems.CollectionChanged += SelectedItems_CollectionChanged;
            BrowserViewModel.Columns.CollectionChanged += Columns_CollectionChanged;
            ListView.CustomSort = BrowserViewModel.Sorter;
            SortListViewItemByName();
        }

        private void BrowserView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue is BrowserViewModel oldModel)
            {
                oldModel.SelectedItems.CollectionChanged -= SelectedItems_CollectionChanged;
                oldModel.Columns.CollectionChanged -= Columns_CollectionChanged;
            }

            if (BrowserViewModel != null)
            {
                AttachViewModel();
            }
        }

        private static void ColumnHeaderContextMenuPropertyChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            var browserView = source as BrowserView;
            if (browserView.GridView != null)
            {
                browserView.GridView.ColumnHeaderContextMenu = e.NewValue as ContextMenu;
            }
        }

        public BrowserItem GetBrowserItems(object originalSource)
        {
            if (originalSource is FrameworkElement element)
            {
                return element.DataContext as BrowserItem;
            }

            return null;
        }

        public bool IsColumnHeaderClicked(object originalSource)
        {
            return ListView.IsColumnHeaderClicked(originalSource);
        }

        public void SwitchListView()
        {
            switch (ViewType)
            {
                case ListViewType.Details:
                    SwitchToView(GridView, defaultStyle);
                    break;

                case ListViewType.Icon:
                    SwitchToView(iconView, tileListViewStyle);
                    break;

                case ListViewType.List:
                    SwitchToView(listView, tileListViewStyle);
                    break;
            }
        }

        public void SwitchToView(ViewBase viewBase, Style style)
        {
            ListView.Style = style;
            ListView.View = viewBase;
        }

        private void Columns_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            GridView.Columns.Clear();

            foreach (var columnInfo in BrowserViewModel.Columns.Where(col => col.DisplayIndex != -1).OrderBy(col => col.DisplayIndex))
            {
                GridView.Columns.Add(CreateGridViewColumn(columnInfo));
            }

            foreach (var columnInfo in BrowserViewModel.Columns.Where(col => col.DisplayIndex == -1))
            {
                GridView.Columns.Add(CreateGridViewColumn(columnInfo));
            }
        }

        private GridViewColumn CreateGridViewColumn(ColumnInfo info)
        {
            var col = new GridViewColumnEx(info.Name, info.CanHide, info.CanReorder);
            col.SetValue(GridViewColumnProperties.NameProperty, info.Name);
            col.Header = info.Text;
            col.CellTemplate = TryFindResource(info.Name) as DataTemplate;
            col.Width = info.InitialWidth;
            col.BindingPath = info.BindingPath;
            col.Visible = info.Visible;
            var binding = BindingOperationsEx.CreateTwoWayBinding(info, "Visible");
            BindingOperations.SetBinding(col, GridViewColumnEx.VisibleProperty, binding);

            if (col.CellTemplate == null)
            {
                col.DisplayMemberBinding = new Binding(info.BindingPath);
            }

            return col;
        }

        private void CreateListStyle()
        {
            defaultStyle = FindResource(typeof(ListViewEx)) as Style;
            iconView = FindResource("iconView") as TileView;
            listView = FindResource("tileView") as TileView;
            GridView = FindResource("detailView") as GridView;
            tileListViewStyle = FindResource(new ComponentResourceKey(iconView.GetType(), "tileViewDSK")) as Style;
            ListView.AllowDrop = true;
            ListViewStyleManager = new ListViewStyleManager(this);
        }

        private void HideComboBox(object sender)
        {
            if (sender is ComboBox comboBox && comboBox.DataContext is BrowserItem item)
            {
                item.EditingQuality = false;
            }
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BrowserViewModel.SelectedItems.CollectionChanged -= SelectedItems_CollectionChanged;

            BrowserViewModel.SelectedItems.ClearAddRange(ListView.SelectedItems.OfType<BrowserItem>());

            BrowserViewModel.SelectedItem = ListView.SelectedIndex >= 0 ? ListView.Items[ListView.SelectedIndex] as BrowserItem : null;
            BrowserViewModel.SelectedItems.CollectionChanged += SelectedItems_CollectionChanged;
        }

        private void QualityControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is Grid grid && grid.DataContext is BrowserItem item)
            {
                item.EditingQuality = true;
            }
        }

        private void QualityControlComboBox_MouseLeave(object sender, MouseEventArgs e)
        {
            HideComboBox(sender);
        }

        private void QualityControlComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            HideComboBox(sender);
        }

        private void RenameTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (sender is TextBox textBox)
            {
                textBox.SelectAll();
            }
        }

        private void RenameTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (sender is TextBox textBox && textBox.DataContext is BrowserItem item)
            {
                if (e.Key == Key.Enter)
                {
                    item.Renaming = false;
                }
                else if (e.Key == Key.Escape)
                {
                    textBox.Text = item.Name;
                    item.Renaming = false;
                }
            }
        }

        private void RenameTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (sender is TextBox textBox && textBox.DataContext is BrowserItem item)
            {
                item.Renaming = false;
                BrowserViewModel.UpdateLongestName();
            }
        }

        private void RenameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox textBox && textBox.Text.Length > BrowserViewModel.LongestName.Length)
            {
                BrowserViewModel.LongestName = textBox.Text;
            }
        }

        private void SelectedItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            ListView.SelectionChanged -= ListView_SelectionChanged;
            ListView.UnselectAll();

            foreach (var item in BrowserViewModel.SelectedItems)
            {
                ListView.SelectedItems.Add(item);
            }

            ListView.ScrollIntoView(BrowserViewModel.SelectedItems.FirstOrDefault());
            ListView.SelectionChanged += ListView_SelectionChanged;
        }

        private void SortListViewItemByName()
        {
            ListView.SortOnProperty(nameof(BrowserItem.Name), System.ComponentModel.ListSortDirection.Ascending, true);
        }

        private void CamioChannelControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is Grid grid && grid.DataContext is BrowserItem item)
            {
                item.EditingCamioChannel = true;
            }
        }

        private void CamioChannelEditControl_LostFocus(object sender, RoutedEventArgs e)
        {
            DisableCamioEditing(sender);
        }

        private void DisableCamioEditing(object sender)
        {
            if (sender is TextBox grid && grid.DataContext is BrowserItem item)
            {
                item.EditingCamioChannel = false;
            }
        }

        private void CamioChannelEditControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return || e.Key == Key.Escape)
            {
                DisableCamioEditing(sender);
            }
            if (sender is TextBox textbox && !textbox.Text.All(char.IsLetterOrDigit))
            {
                e.Handled = false;
            }
        }
    }
}