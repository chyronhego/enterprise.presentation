﻿namespace ChyronHego.Enterprise.Presentation.Views
{    
    using System.Windows;
    using System.Windows.Controls;
    using ChyronHego.Enterprise.Presentation.ViewModels;

    public class BaseView : UserControl
    {
        public BaseView()
        {
            DataContextChanged += BaseView_DataContextChanged;
        }

        private void BaseView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (DataContext is BaseViewModel model)
            {
                model.Dispatcher = Dispatcher;
                WpfExceptionHandler.Initialize(Dispatcher);
            }
        }
    }
}
