﻿namespace ChyronHego.Enterprise.Presentation.Views
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using ChyronHego.Enterprise.Presentation.Controls.Properties;
    using ChyronHego.Enterprise.Presentation.Converters;
    using ChyronHego.Enterprise.Presentation.Objects;
    using ChyronHego.Enterprise.Presentation.Styles;
    using ChyronHego.Enterprise.Presentation.Styles.Properties;
    using ChyronHego.Enterprise.Presentation.ViewModels;

    /// <summary>
    /// Interaction logic for WorkflowMonitorView.xaml
    /// </summary>
    public partial class LogView : BaseView
    {
        private bool singleLineText = true;

        public LogViewModel LogViewModel { get; private set; }

        public LogView()
        {
            InitializeComponent();
            InitializeData();
        }

        public bool SingleLineItem
        {
            get
            {
                return singleLineText;
            }

            set
            {
                if (singleLineText != value)
                {
                    singleLineText = value;
                    RepopulateColumns();
                }
            }
        }

        public LogView(LogViewModel model)
        {
            InitializeComponent();
            DataContext = model;
            InitializeData();
            ListViewStyleManager = new ListViewStyleManager(this);
        }

        private void InitializeData()
        {
            LogViewModel = DataContext as LogViewModel;
            Loaded += LogView_Loaded;
            LogViewModel.Columns.CollectionChanged += Columns_CollectionChanged;
        }

        private void LogView_Loaded(object sender, RoutedEventArgs e)
        {
            RepopulateColumns();
        }

        private void Columns_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            RepopulateColumns();
        }

        private void RepopulateColumns()
        {
            if (LogViewModel != null)
            {
                GridView.Columns.Clear();

                foreach (var columnInfo in LogViewModel.Columns)
                {
                    GridView.Columns.Add(CreateGridViewColumn(columnInfo));
                }
            }
        }

        private GridViewColumn CreateGridViewColumn(ColumnInfo info)
        {
            var col = new GridViewColumn();
            col.SetValue(GridViewColumnProperties.NameProperty, info.Name);
            col.Header = info.Text;
            col.CellTemplate = TryFindResource(info.Name) as DataTemplate;
            col.Width = info.InitialWidth;

            if (info.DisplayBindingIndex != -1 && col.CellTemplate == null)
            {
                var binding = new Binding($"[{info.DisplayBindingIndex}]");
                if (SingleLineItem)
                {
                    binding.Converter = new SingleLineConverter();
                }
                col.DisplayMemberBinding = binding;
            }

            return col;
        }

        public ListViewStyleManager ListViewStyleManager { get; private set; }
    }
}