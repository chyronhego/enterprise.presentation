﻿using ChyronHego.Enterprise.Presentation.Collections;

namespace ChyronHego.Enterprise.Presentation.Objects
{
    public class LogItem : ObservableCollectionEx<string>
    {
        public object Tag { get; set; }
    }
}