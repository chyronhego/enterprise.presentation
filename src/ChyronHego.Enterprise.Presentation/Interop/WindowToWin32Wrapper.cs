﻿namespace ChyronHego.Enterprise.Presentation.Interop
{
    using System;
    using System.Windows;
    using System.Windows.Interop;
    using IWin32Window = System.Windows.Forms.IWin32Window;

    public class WindowToWin32Wrapper : IWin32Window
    {
        public WindowToWin32Wrapper(IntPtr handle)
        {
            Handle = handle;
        }

        public WindowToWin32Wrapper(Window window)
        {
            Handle = (window == null) ? IntPtr.Zero : new WindowInteropHelper(window).Handle;
        }

        public IntPtr Handle { get; }
    }

    public static class WindowToWin32Extensions
    {
        public static IWin32Window AsIWin32Window(this Window window)
        {
            return new WindowToWin32Wrapper(window);
        }
    }
}
