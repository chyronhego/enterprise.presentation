﻿namespace ChyronHego.Enterprise.Presentation.Interop
{
    using System.Windows.Forms;
    using System.Windows.Forms.Integration;

    public class ProcessKeyElementHost : ElementHost
    {
        public event KeyEventHandler ProcessKeyDown;

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            var arguments = new KeyEventArgs(keyData);

            ProcessKeyDown?.Invoke(this, arguments);

            if (!arguments.Handled)
            {
                return base.ProcessCmdKey(ref msg, keyData);
            }
            else
            {
                return false;
            }
        }
    }
}
