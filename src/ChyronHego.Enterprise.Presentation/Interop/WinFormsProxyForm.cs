﻿namespace ChyronHego.Enterprise.Presentation.Interop
{
    using System;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Forms;
    using WinFormsDrawing = System.Drawing;

    public partial class WinFormsProxyForm : Form
    {
        private readonly Window parentWindow;

        public WinFormsProxyForm(Window parentWindow)
        {
            this.parentWindow = parentWindow;
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.None;
            ShowInTaskbar = false;
            StartPosition = FormStartPosition.CenterScreen;
            Load += winFormsProxyForm_Load;
            TopLevel = false;
            AttachParentWindowEvents();
        }

        private void winFormsProxyForm_Load(object sender, EventArgs e)
        {
            this.Size = new WinFormsDrawing.Size(0,0);
        }

        private void AttachParentWindowEvents()
        {
            if (parentWindow == null)
            {
                return;
            }

            parentWindow.Activated += parentWindow_Activated;
            parentWindow.Closed += parentWindow_Closed;
            parentWindow.Closing += parentWindow_Closing;
        }

        private void DetachParentWindowEvents()
        {
            if (parentWindow == null)
            {
                return;
            }

            parentWindow.Activated -= parentWindow_Activated;
            parentWindow.Closed -= parentWindow_Closed;
            parentWindow.Closing -= parentWindow_Closing;
        }

        private void parentWindow_Closing(object sender, CancelEventArgs e)
        {
            OnClosing(e);
        }

        private void parentWindow_Closed(object sender, EventArgs e)
        {
            OnClosed(e);
            this.Close();
            DetachParentWindowEvents();
        }

        private void parentWindow_Activated(object sender, EventArgs e)
        {
            OnActivated(e);
        }
    }
}
