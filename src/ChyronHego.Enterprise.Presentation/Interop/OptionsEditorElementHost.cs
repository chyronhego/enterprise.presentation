﻿namespace ChyronHego.Enterprise.Presentation.Interop
{
    using System;
    using System.Windows;
    using System.Windows.Forms.Integration;
    using ChyronHego.Enterprise.Infrastructure.Options;

    public class OptionsEditorElementHost<T> : ElementHost, IOptionsEditor where T : UIElement, IOptionsEditor
    {
        private readonly T editor;

        public OptionsEditorElementHost()
            : this(Activator.CreateInstance<T>())
        {
        }

        public OptionsEditorElementHost(T editor)
        {
            this.editor = editor;

            Child = editor;
        }

        IOptions IOptionsEditor.Options
        {
            get => editor.Options;
            set => editor.Options = value;
        }
    }
}
