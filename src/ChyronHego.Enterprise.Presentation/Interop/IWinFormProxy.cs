﻿namespace ChyronHego.Enterprise.Presentation.Interop
{
    using System.Windows.Forms;

    public interface IWinFormProxy
    {
        Form WinForm { get; }
    }
}
